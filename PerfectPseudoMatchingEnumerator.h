//
// Created by benedikt on 10/23/18.
//

#ifndef PPM_TESTING_PERFECTPSEUDOMATCHINGITERATOR_H
#define PPM_TESTING_PERFECTPSEUDOMATCHINGITERATOR_H

#include "PerfectPseudoMatching.h"
#include <deque>
#include <vector>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/properties.hpp>


template<class Graph>
class PerfectPseudoMatchingEnumerator {
  public:
    PerfectPseudoMatchingEnumerator(const Graph& graph, bool includePseudo = true);

    std::deque<PerfectPseudoMatching<Graph>> getAll();

    template<class Callback>
    std::deque<PerfectPseudoMatching<Graph>> getAllOrFirstSatisfying(Callback check);

  private:
    const Graph& graph;
    bool includePseudo;

    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
    typedef typename boost::graph_traits<Graph>::edge_descriptor Edge;
    typedef typename boost::graph_traits<Graph>::vertex_iterator vertex_iter;

    template<class VisitedMap, class Callback>
    bool
    recurse(std::deque<PerfectPseudoMatching<Graph>>& partitions, std::vector<Component<Vertex, Edge>>& partial,
            vertex_iter
            v_it, vertex_iter
            it_end,
            VisitedMap visited, const Callback& check);

    template<class VisitedMap, class Callback>
    bool
    tryClaw(std::deque<PerfectPseudoMatching<Graph>>& partitions, std::vector<Component<Vertex, Edge>>& partial,
            vertex_iter v_it, vertex_iter it_end,
            VisitedMap visited, const Callback& check, Vertex center);
};

template<class Graph>
PerfectPseudoMatchingEnumerator<Graph>::PerfectPseudoMatchingEnumerator(const Graph& graph, bool includePseudo) : graph(
    graph), includePseudo(includePseudo) {}

template<class Graph>
std::deque<PerfectPseudoMatching<Graph>> PerfectPseudoMatchingEnumerator<Graph>::getAll() {
  return this->getAllOrFirstSatisfying([](const PerfectPseudoMatching<Graph>& x) { return false; });
}

template<class Graph>
template<class Callback>
std::deque<PerfectPseudoMatching<Graph>> PerfectPseudoMatchingEnumerator<Graph>::getAllOrFirstSatisfying(
    Callback check) {
  std::deque<PerfectPseudoMatching<Graph>> result;
  std::vector<Component<Vertex, Edge>> current;
  current.reserve(num_vertices(this->graph));
  typedef typename boost::property_map<Graph, boost::vertex_index_t>::type VertexIndexMap;
  auto vertex_id = boost::get(boost::vertex_index, this->graph);
  typedef boost::iterator_property_map<std::vector<bool>::iterator, VertexIndexMap> IterMap;
  auto visited = std::vector<bool>(num_vertices(this->graph), false);


  auto vp = vertices(this->graph);
  this->recurse(result, current, vp.first, vp.second, IterMap(visited.begin(), vertex_id), check);
  return result;
}


template<class Graph>
template<class VisitedMap, class Callback>
bool PerfectPseudoMatchingEnumerator<Graph>::recurse(std::deque<PerfectPseudoMatching<Graph>>& partitions,
                                                     std::vector<Component<Vertex, Edge>>& partial, vertex_iter v_it,
                                                     vertex_iter
                                                     it_end,
                                                     VisitedMap visited, const Callback& check) {

  for (; (v_it != it_end) && boost::get(visited, *v_it); v_it++) {}
  if (v_it == it_end) {
    //we found a perfect pseudo matching
    partitions.push_back(PerfectPseudoMatching<Graph>(this->graph, partial));
    return check(partitions.back());
  }

  Vertex v = *v_it;

  if (this->includePseudo) {
    //use v as center of a claw
    if (this->tryClaw(partitions, partial, v_it, it_end, visited, check, v)) { return true; }

    //use the neighbors of v as a center of a claw
    auto ap = adjacent_vertices(v, this->graph);
    for (auto it = ap.first; it != ap.second; it++) {
      if (this->tryClaw(partitions, partial, v_it, it_end, visited, check, *it)) { return true; }
    }
  }

  //use the edges incident to v
  auto ep = out_edges(v, this->graph);
  for (auto it = ep.first; it != ep.second; it++) {
    Edge e = *it;
    Vertex w = target(e, this->graph);
    if (w == v) {
      w = source(e, this->graph);
    }
    if (!get(visited, w)) {
      put(visited, v, true);
      put(visited, w, true);
      partial.push_back(Component<Vertex, Edge>(e));
      auto it_copy = v_it;
      it_copy++;
      if (this->recurse(partitions, partial, it_copy, it_end, visited, check)) {
        return true;
      }
      partial.pop_back();
      put(visited, v, false);
      put(visited, w, false);
    }
  }

  return false;
}

template<class Graph>
template<class VisitedMap, class Callback>
bool
PerfectPseudoMatchingEnumerator<Graph>::tryClaw(std::deque<PerfectPseudoMatching<Graph>>& partitions,
                                                std::vector<Component<Vertex, Edge>>&
                                                partial,
                                                vertex_iter v_it, vertex_iter it_end, VisitedMap visited,
                                                const Callback& check, Vertex center) {
  if (get(visited, center)) {
    return false;
  }

  auto ap = adjacent_vertices(center, this->graph);
  for (auto it = ap.first; it != ap.second; it++) {
    if (get(visited, *it)) {
      return false;
    }
  }
  put(visited, center, true);
  for (auto it = ap.first; it != ap.second; it++) {
    put(visited, *it, true);
  }
  partial.push_back(Component<Vertex, Edge>(center));
  auto it_copy = v_it;
  it_copy++;
  bool res = this->recurse(partitions, partial, it_copy, it_end, visited, check);
  partial.pop_back();
  put(visited, center, false);
  for (auto it = ap.first; it != ap.second; it++) {
    put(visited, *it, false);
  }
  return res;
}



#endif //PPM_TESTING_PERFECTPSEUDOMATCHINGITERATOR_H
