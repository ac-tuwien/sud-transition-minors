//
// Created by benedikt on 10/23/18.
//

#ifndef PPM_TESTING_PERFECTPSEUDOMATCHING_H
#define PPM_TESTING_PERFECTPSEUDOMATCHING_H

#include <vector>
#include "Component.h"
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/property_map/property_map.hpp>
#include <set>
#include <unordered_map>
#include <boost/bimap.hpp>
#include "TransitionedGraph.h"

enum Property {
    Planar, K5MinorFree, BadK5MinorFree, ContainsCCD
};
enum Status {
    True, False, Unknown
};

namespace PPM {
  const std::set<Property> properties{Planar, K5MinorFree, BadK5MinorFree, ContainsCCD};
  namespace priv {
    boost::bimap<Property, Property> get_implications() {
      boost::bimap<Property, Property> res;
      res.insert(boost::bimap<Property, Property>::value_type(Planar, K5MinorFree));
      res.insert(boost::bimap<Property, Property>::value_type(K5MinorFree, BadK5MinorFree));
      res.insert(boost::bimap<Property, Property>::value_type(BadK5MinorFree, ContainsCCD));
      return res;
    }
  }
  boost::bimap<Property, Property> implications = priv::get_implications();
  const std::unordered_map<Property, std::string, std::hash<int>> property_names({{Planar,         "planarizing"},
                                                                                  {K5MinorFree,    "K5-Minor-free"},
                                                                                  {BadK5MinorFree, "Bad-K5-Minor-free"},
                                                                                  {ContainsCCD,    "CCD-containing"}});
}

template<class Graph>
class PerfectPseudoMatching {
  public:
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
    typedef typename boost::graph_traits<Graph>::edge_descriptor Edge;

    PerfectPseudoMatching(const Graph& graph, const std::vector<Component<Vertex, Edge>> components);


    const TransitionedGraph<Graph>& getOrCreateContracted();

    Status getProperty(Property prop) const;

    void setProperty(Property prop, bool value);

    bool isIsPerfectMatching() const {
      return isPerfectMatching;
    }

    std::string toString() const;

    bool complementIsCycle(std::string& representation) const;

    std::set<int> clawCenters() const;

    template<class G>
    friend std::ostream& operator<<(std::ostream& os, const PerfectPseudoMatching<G>& o);

    const Graph& getGraph() const {
      return graph;
    }

  private:
    bool isPerfectMatching;
    const Graph& graph;
    std::vector<Component<Vertex, Edge>> components;
    TransitionedGraph<Graph> contracted;
    std::vector<Vertex> original_v_to_contracted;
    std::vector<Edge> contracted_e_to_original;
    std::unordered_map<Property, bool, std::hash<int>> known_properties;
};

template<class Graph>
PerfectPseudoMatching<Graph>::PerfectPseudoMatching(const Graph& graph,
                                                    const std::vector<Component<Vertex, Edge>> components)
    : isPerfectMatching(true), graph(graph), components(components), contracted(TransitionedGraph<Graph>()) {
      for (auto& component: this->components) {
        if (component.getType() == claw) {
          isPerfectMatching = false;
          break;
        }
      }
    }

template<class Graph>
const TransitionedGraph<Graph>& PerfectPseudoMatching<Graph>::getOrCreateContracted() {
  if (num_vertices(contracted.getGraph()) == 0) {
    //initialize contraction
    auto vertex_id = boost::get(boost::vertex_index, graph);
    original_v_to_contracted = std::vector<Vertex>(num_vertices(graph));
    contracted_e_to_original.reserve(num_edges(graph) - components.size());

    std::set<Vertex> contractedV;
    std::size_t edge_index = 0;
    std::unordered_map<int, Edge> original_e_to_contracted;
    auto edge_id = boost::get(boost::edge_index, contracted.getGraph());
    std::vector<Transition<Vertex, Edge>> transitions;
    for (const Component<Vertex, Edge>& component: components) {
      Vertex v = add_vertex(contracted.getGraph());
      int v_ind = num_vertices(contracted.getGraph()) - 1;
      switch (component.getType()) {
      case edge: {
        Edge e = component.getE();
        auto vertices = {source(e, graph), target(e, graph)};

        for (const Vertex& w: vertices) {
          original_v_to_contracted[get(vertex_id, w)] = v;
          contractedV.insert(w);
          auto ep = out_edges(w, graph);
          Edge es[2];
          int index = 0;
          for (auto eit = ep.first; eit != ep.second; eit++) {
            if (*eit != e) {
              Vertex u = target(*eit, graph);
              if (u == w) {
                u = source(*eit, graph);
              }
              es[index++] = *eit;
              int v2_ind = get(vertex_id, u);
              if (contractedV.count(u) > 0 && original_v_to_contracted[v2_ind] != v) {

                Edge newEdge = add_edge(v, original_v_to_contracted[v2_ind], edge_index++, contracted.getGraph()).first;
                contracted_e_to_original.push_back(*eit);
                original_e_to_contracted.insert(std::make_pair(get(edge_id, *eit), newEdge));
              }
            }
          }
          transitions.push_back(Transition<Vertex, Edge>(v, es[0], es[1]));
        }
        break;
      }
      case claw: {
        Vertex center = component.getV();
        original_v_to_contracted[get(vertex_id, center)] = v_ind;
        contractedV.insert(center);
        auto ap = adjacent_vertices(center, graph);
        for (auto wit = ap.first; wit != ap.second; wit++) {
          Vertex w = *wit;
          original_v_to_contracted[get(vertex_id, w)] = v;
          contractedV.insert(w);
          auto ep = out_edges(w, graph);
          Edge es[2];
          int index = 0;
          for (auto eit = ep.first; eit != ep.second; eit++) {
            Vertex u = target(*eit, graph);
            if (u == w) {
              u = source(*eit, graph);
            }
            if (u != center) {
              es[index++] = *eit;
              int v2_ind = get(vertex_id, u);
              if (contractedV.count(u) > 0 && original_v_to_contracted[v2_ind] != v) {
                Edge newEdge = add_edge(v, original_v_to_contracted[v2_ind], edge_index++, contracted.getGraph()).first;
                contracted_e_to_original.push_back(*eit);
                original_e_to_contracted.insert(std::make_pair(get(edge_id, *eit), newEdge));
              }
            }
          }
          transitions.push_back(Transition<Vertex, Edge>(v, es[0], es[1]));
        }
        break;
      }
      }
    }
    for (auto& transition: transitions) {
      transition.setE1(original_e_to_contracted.at(get(edge_id, transition.getE1())));
      assert(source(transition.getE1(), contracted.getGraph()) == transition.getV() ||
             target(transition.getE1(), contracted.getGraph()) == transition.getV());
      transition.setE2(original_e_to_contracted.at(get(edge_id, transition.getE2())));
      assert(source(transition.getE2(), contracted.getGraph()) == transition.getV() ||
             target(transition.getE2(), contracted.getGraph()) == transition.getV());
      assert(transition.getE2() != transition.getE1());
    }
    contracted.setTransitions(transitions);
    assert(num_vertices(contracted.getGraph()) == components.size());
    assert(num_edges(contracted.getGraph()) ==
           num_edges(graph) + num_vertices(contracted.getGraph()) - num_vertices(graph));
  }
  return contracted;
}

template<class Graph>
void PerfectPseudoMatching<Graph>::setProperty(Property prop, bool value) {
  if (known_properties.count(prop) == 1 && known_properties[prop] == value) {
    return;
  }
  assert (known_properties.count(prop) == 0);
  known_properties[prop] = value;
  if (value) {
    if (PPM::implications.left.count(prop) > 0) {
      setProperty(PPM::implications.left.at(prop), value);
    }
  } else {
    if (PPM::implications.right.count(prop) > 0) {
      setProperty(PPM::implications.right.at(prop), value);
    }
  }
}

template<class Graph>
Status PerfectPseudoMatching<Graph>::getProperty(Property prop) const {
  if (known_properties.count(prop) == 0) {
    return Unknown;
  } else {
    return known_properties.at(prop) ? Status::True : Status::False;
  }
}

template<class Graph>
std::ostream& operator<<(std::ostream& os, const PerfectPseudoMatching<Graph>& o) {
  os << "(";
  bool first = true;
  for (auto& component: o.components) {
    if (!first) {
      os << ", ";
    } else {
      first = false;
    }
    os << component;
  }
  os << ")";
  return os;
}

template<class Graph> bool PerfectPseudoMatching<Graph>::complementIsCycle(std::string& representation) const {
  std::set<int> contracted_edge_ids;
  auto edge_id = boost::get(boost::edge_index, graph);
  Vertex start_vertex = 0;
  for (const Component<Vertex, Edge>& component: components) {
    switch (component.getType()) {
    case edge: {
      contracted_edge_ids.insert(get(edge_id, component.getE()));
      break;
    }
    case claw: {
      Vertex center = component.getV();
      if (center == start_vertex) {
        start_vertex = *adjacent_vertices(center, graph).first;
      }
      auto ep = out_edges(center, graph);
      for (auto eit = ep.first; eit != ep.second; eit++) {
        contracted_edge_ids.insert(get(edge_id, *eit));
      }
      break;
    }
    }
  }

  auto remaining_edges = num_edges(graph);
  remaining_edges -= contracted_edge_ids.size();

  //try constructing cycle
  size_t cycle_length = 0;
  size_t last_edge_id = -1;
  Vertex current = start_vertex;
  representation = std::to_string(current);
  while (cycle_length == 0 || current != start_vertex) {
    auto ep = out_edges(current, graph);
    for (auto eit = ep.first; eit != ep.second; eit++) {
      auto id = get(edge_id, *eit);
      if (id != last_edge_id && contracted_edge_ids.count(id) == 0) {
        cycle_length++;
        Vertex u = target(*eit, graph);
        if (u == current) {
          u = source(*eit, graph);
        }
        current = u;
        representation += "-" + std::to_string(current);
        last_edge_id = id;
        break;
      }
    }
  }

  return cycle_length == remaining_edges;
}

template<class Graph> std::set<int> PerfectPseudoMatching<Graph>::clawCenters() const {
  std::set<int> res;
  for (const Component<Vertex, Edge>& component: components) {
    switch (component.getType()) {
    case edge: {
      //do nothing
      break;
    }
    case claw: {
      res.insert(component.getV());
      break;
    }
    }
  }
  return res;
}

#endif //PPM_TESTING_PERFECTPSEUDOMATCHING_H
