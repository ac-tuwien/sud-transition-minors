//
// Created by benedikt on 10/24/18.
//

#ifndef PPM_TESTING_SUDMINORSOLVERSAT_H
#define PPM_TESTING_SUDMINORSOLVERSAT_H

#include "simp/SimpSolver.h"
#include <vector>
#include "boost/graph/graph_traits.hpp"
#include <boost/functional/hash.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/named_graph.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include "TransitionedGraph.h"
#include <boost/graph/directed_graph.hpp>
#include "satLib/Formula.h"
#include "satLib/helpers.h"
#include "satLib/GlucoseConnector.h"
#include "AutomorphismGroup.h"
#include "ContainsSUDMinorMIP.h"

template<class Graph>
std::vector<int> getOrbIndizes(const AutomorphismGroup<Graph>& aut, int n) {
  std::vector<int> res(n);
  for (size_t i = 0; i < aut.getOrbits().size(); i++) {
    for (auto v: aut.getOrbits()[i]) {
      res[v] = i;
    }
  }
  return res;
}

template<class Graph>
bool containsSUDMinor(const TransitionedGraph<Graph>& G, const TransitionedGraph<Graph>& H, double& modellingTime,
                         double& solvingTime, bool sym1, bool sym2, bool mip = false, double timelimit = 0.0,
                         bool* hasSymmetries = nullptr, std::string unsatFile = "") {
  if (hasSymmetries != nullptr) {
    *hasSymmetries = false;
  }
  if (mip) {
    return containsSUDMinorMIP(G, H, modellingTime, solvingTime, sym1, sym2, timelimit);
  } else {
    return containsSUDMinorSAT(G, H, modellingTime, solvingTime, sym1, sym2, timelimit, hasSymmetries, unsatFile);
  }
}

template<class Graph>
bool containsSUDMinorSAT(const TransitionedGraph<Graph>& G, const TransitionedGraph<Graph>& H, double& modellingTime,
                      double& solvingTime, bool sym1, bool sym2, double timelimit, bool* hasSymmetries = nullptr,
                      std::string unsatFile = "") {
  std::clock_t c_start = std::clock();

  auto model = satLib::Formula::getTrue();

  typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
  typedef boost::property<boost::edge_index_t, std::size_t> DEdgeProperty;
  typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, boost::no_property, DEdgeProperty> DGraph;
  typedef typename boost::graph_traits<DGraph>::vertex_descriptor DVertex;
  DGraph simple;

  std::vector<DVertex> simple_vertices;
  simple_vertices.reserve(num_vertices(G.getGraph()));
  for (size_t i = 0; i < num_vertices(G.getGraph()); i++) {
    simple_vertices.push_back(add_vertex(simple));
  }
  auto es = edges(G.getGraph());
  size_t id = 0;
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto v1 = source(*e_it, G.getGraph());
    auto v2 = target(*e_it, G.getGraph());
    add_edge(v1, v2, id++, simple);
    add_edge(v2, v1, id++, simple);
  }

  /*
   * Variables
   */
  auto iota = satLib::createPartialFunction(num_vertices(G.getGraph()), num_vertices(H.getGraph()), model, "i");
  model &= satLib::surjective(iota);

  auto kappa = satLib::createPartialFunction(num_edges(G.getGraph()), num_edges(H.getGraph()), model, "k");
  model &= satLib::injective(kappa);
  model &= satLib::surjective(kappa);

  auto theta = satLib::createPartialFunction(num_edges(G.getGraph()), num_vertices(H.getGraph()), model, "t");
  model &= satLib::injective(theta);

  auto tw = satLib::createFunction(num_vertices(H.getGraph()), G.getTransitions().size(), model, "T");
  model &= satLib::injective(tw);

  satLib::Relation sw(num_vertices(H.getGraph()));
  for (size_t i = 0; i < num_vertices(H.getGraph()); i++) {
    auto imageSize = H.getTransitionIndices(i).size();
    sw[i].reserve(imageSize);
    for (size_t j = 0; j < imageSize; j++) {
      std::string name;
      name = "S(" + std::to_string(i) + ") = " + std::to_string(j);
      sw[i].push_back(satLib::Variable(name));
    }
  }
  model &= satLib::partial(sw);
  model &= satLib::function(sw);

  std::vector<std::vector<std::vector<satLib::Variable>>> ca(num_edges(simple));
  auto as = edges(simple);
  auto arc_id = boost::get(boost::edge_index, simple);
  auto sv_id = boost::get(boost::vertex_index, simple);
  for (auto a_it = as.first; a_it != as.second; a_it++) {
    size_t i = get(arc_id, *a_it);
    ca[i].reserve(num_vertices(H.getGraph()));
    for (size_t j = 0; j < num_vertices(H.getGraph()); j++) {
      ca[i].push_back(std::vector<satLib::Variable>());
      ca[i][j].reserve(2);
      for (int k = 0; k < 2; k++) {
        std::string name;
        name = "(" + std::to_string(get(sv_id, source(*a_it, simple))) + "," +
               std::to_string(get(sv_id, target(*a_it, simple))) + ") in E(C_" + std::to_string(j) + "^" +
               std::to_string(k) + ")";
        ca[i][j].push_back(satLib::Variable(name));
      }
    }
  }

  std::vector<std::vector<std::vector<satLib::Variable>>> cv(num_vertices(G.getGraph()));
  for (size_t i = 0; i < num_vertices(G.getGraph()); i++) {
    cv[i].reserve(num_vertices(H.getGraph()));
    for (size_t j = 0; j < num_vertices(H.getGraph()); j++) {
      cv[i].push_back(std::vector<satLib::Variable>());
      cv[i][j].reserve(2);
      for (int k = 0; k < 2; k++) {
        std::string name;
        name = std::to_string(i) + " in V(C_" + std::to_string(j) + "^" + std::to_string(k) + ")";
        cv[i][j].push_back(satLib::Variable(name));
      }
    }
  }

  // use transitive closure to enforce acyclicity
  // (see https://link.springer.com/content/pdf/10.1007/978-3-319-11558-0_10.pdf)
  auto tr = satLib::createRelation(num_vertices(G.getGraph()), num_vertices(G.getGraph()), " pre ", false);

  //tree constraints
  auto edge_id_G = boost::get(boost::edge_index, G.getGraph());
  auto vertex_id_G = boost::get(boost::vertex_index, G.getGraph());
  auto edge_id_H = boost::get(boost::edge_index, H.getGraph());
  auto vertex_id_H = boost::get(boost::vertex_index, H.getGraph());
  auto vs = vertices(G.getGraph());
  auto ws = vertices(H.getGraph());
  for (auto v_it = vs.first; v_it != vs.second; v_it++) {
    int v_ind = get(vertex_id_G, *v_it);
    for (auto w_it = ws.first; w_it != ws.second; w_it++) {
      int w_ind = get(vertex_id_G, *w_it);
      for (int i = 0; i < 2; i++) {
        auto v_eq_pi1_Tw = satLib::Formula::getFalse();
        for (auto& ind: G.getTransitionIndices(*v_it)) {
          v_eq_pi1_Tw |= tw[w_ind][ind];
        }
        //v in V(C_w^i) -> v = pi1(T_w) or exactly one ingoing arc
        //we can also allow to have more than one ingoing arc
        auto one_ingoing_arc = satLib::Formula::getTrue();

        //first at most one ingoing arc this is not needed TODO test if its better without that
        auto in = boost::in_edges(*v_it, simple);
        for (auto it = in.first; it != in.second; it++) {
          int a1_ind = get(arc_id, *it);
          auto it2 = it;
          it2++;
          for (; it2 != in.second; it2++) {
            int a2_ind = get(arc_id, *it2);
            one_ingoing_arc &= -ca[a1_ind][w_ind][i] | -ca[a2_ind][w_ind][i];
          }
        }

        //second at least one ingoing arc
        auto at_least_one = satLib::Formula::getFalse();
        for (auto it = in.first; it != in.second; it++) {
          int a_ind = get(arc_id, *it);
          at_least_one |= ca[a_ind][w_ind][i];
        }
        one_ingoing_arc &= at_least_one;

        model &= cv[v_ind][w_ind][i] >> (v_eq_pi1_Tw | one_ingoing_arc);

        //v = pi1(T_w) -> v in V(C_w^i) and no ingoing edge
        auto no_ingoing_arc = satLib::Formula::getTrue();
        for (auto it = in.first; it != in.second; it++) {
          int a_ind = get(arc_id, *it);
          no_ingoing_arc &= -ca[a_ind][w_ind][i];
        }

        model &= v_eq_pi1_Tw >> (cv[v_ind][w_ind][i] & no_ingoing_arc);

        //v not in V(C_w^i) -> no ingoing edge & no outgoing arc
        auto out = boost::out_edges(*v_it, simple);
        auto no_outgoing_arc = satLib::Formula::getTrue();
        for (auto it = out.first; it != out.second; it++) {
          int a_ind = get(arc_id, *it);
          no_outgoing_arc &= -ca[a_ind][w_ind][i];
        }

        model &= -cv[v_ind][w_ind][i] >> (no_ingoing_arc & no_outgoing_arc);
      }
    }
  }

  // transitive closure of tree arcs
  auto closure = satLib::createRelation(num_vertices(simple), num_vertices(simple), "->", false);

  for (auto a_it = as.first; a_it != as.second; a_it++) {
    size_t a = get(arc_id, *a_it);
    auto v1 = get(sv_id, source(*a_it, simple));
    auto v2 = get(sv_id, target(*a_it, simple));

    //a in E(C_w^i) for any w and i => v1 pre v2
    auto a_in_any_EC_wi = satLib::Formula::getFalse();
    for (auto w_it = ws.first; w_it != ws.second; w_it++) {
      int w_ind = get(vertex_id_G, *w_it);
      for (int i = 0; i < 2; i++) {
        a_in_any_EC_wi |= ca[a][w_ind][i];
      }
    }

    model &= a_in_any_EC_wi >> closure[v1][v2];

    //v1 pre v2 and v2 pre v3 => v1 pre v3 (i.e. transitivity)
    for (auto v_it = vs.first; v_it != vs.second; v_it++) {
      size_t v3 = get(sv_id, *v_it);
      model &= (closure[v1][v2] & closure[v2][v3]) >> closure[v1][v3];
    }
    // not (v1 pre v2 and v2 pre v1)
    model &= -(closure[v1][v2] & closure[v2][v1]);
  }

  // eq (5)
  auto fs = edges(H.getGraph());
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto e = get(edge_id_G, *e_it);
    auto v1 = get(vertex_id_G, source(*e_it, G.getGraph()));
    auto v2 = get(vertex_id_G, target(*e_it, G.getGraph()));
    for (auto f_it = fs.first; f_it != fs.second; f_it++) {
      auto f = get(edge_id_H, *f_it);
      auto w1 = get(vertex_id_H, source(*f_it, H.getGraph()));
      auto w2 = get(vertex_id_H, target(*f_it, H.getGraph()));
      model &= kappa[e][f] >> ((iota[v1][w1] & iota[v2][w2]) | (iota[v1][w2] & iota[v2][w1]));
    }
  }

  // eq (6)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (auto v_it = vs.first; v_it != vs.second; v_it++) {
      auto v = get(vertex_id_G, *v_it);
      model &= (cv[v][w][0] | cv[v][w][1]).equiv(iota[v][w]);
    }
  }

  // eq (7)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (auto v_it = vs.first; v_it != vs.second; v_it++) {
      auto v = get(vertex_id_G, *v_it);
      auto v_eq_pi1_Tw = satLib::Formula::getFalse();
      for (auto& ind: G.getTransitionIndices(*v_it)) {
        v_eq_pi1_Tw |= tw[w][ind];
      }
      model &= v_eq_pi1_Tw.equiv(cv[v][w][0] & cv[v][w][1]);
    }
  }

  // eq (8)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (auto e_it = es.first; e_it != es.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      auto e_in_pi2_Tw = satLib::Formula::getFalse();
      for (auto& ind: G.getTransitionIndices(source(*e_it, G.getGraph()))) {
        if (get(edge_id_G, G.getTransitions()[ind].getE1()) == e ||
            get(edge_id_G, G.getTransitions()[ind].getE2()) == e) {
          e_in_pi2_Tw |= tw[w][ind];
        }
      }
      for (auto& ind: G.getTransitionIndices(target(*e_it, G.getGraph()))) {
        if (get(edge_id_G, G.getTransitions()[ind].getE1()) == e ||
            get(edge_id_G, G.getTransitions()[ind].getE2()) == e) {
          e_in_pi2_Tw |= tw[w][ind];
        }
      }
      auto k_e_in_pi2_Sw = satLib::Formula::getFalse();
      for (size_t i = 0; i < H.getTransitionIndices(*w_it).size(); i++) {
        auto& ind = H.getTransitionIndices(*w_it)[i];
        auto f1 = get(edge_id_H, H.getTransitions()[ind].getE1());
        auto f2 = get(edge_id_H, H.getTransitions()[ind].getE2());
        k_e_in_pi2_Sw |= sw[w][i] & (kappa[e][f1] | kappa[e][f2]);
      }
      auto v1 = source(*e_it, G.getGraph());
      auto v2 = target(*e_it, G.getGraph());
      auto a1 = get(arc_id, boost::edge(v1, v2, simple).first);
      auto a2 = get(arc_id, boost::edge(v2, v1, simple).first);
      model &= e_in_pi2_Tw >> (k_e_in_pi2_Sw | theta[e][w] | ca[a1][w][0] | ca[a2][w][0]);
    }
  }

  //eq (9)a
  for (size_t Ti = 0; Ti < G.getTransitions().size(); Ti++) {
    auto v = G.getTransitions()[Ti].getV();
    auto out = out_edges(v, G.getGraph());
    for (auto e_it = out.first; e_it != out.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      if (e != get(edge_id_G, G.getTransitions()[Ti].getE1()) && e != get(edge_id_G, G.getTransitions()[Ti].getE2())) {
        //e is not in pi2(T)
        for (auto w_it = ws.first; w_it != ws.second; w_it++) {
          auto w = get(vertex_id_H, *w_it);
          for (size_t Si = 0; Si < H.getTransitionIndices(*w_it).size(); Si++) {
            auto S_ind = H.getTransitionIndices(*w_it)[Si];
            auto f1 = get(edge_id_H, H.getTransitions()[S_ind].getE1());
            auto f2 = get(edge_id_H, H.getTransitions()[S_ind].getE2());
            model &= -(tw[w][Ti] & sw[w][Si] & (kappa[e][f1] | kappa[e][f2]));
          }
        }
      }
    }
  }

  //eq (9)b
  for (size_t Ti = 0; Ti < G.getTransitions().size(); Ti++) {
    for (auto e_it = es.first; e_it != es.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      if (e != get(edge_id_G, G.getTransitions()[Ti].getE1()) && e != get(edge_id_G, G.getTransitions()[Ti].getE2())) {
        //e is not in pi2(T)
        for (auto w_it = ws.first; w_it != ws.second; w_it++) {
          auto w = get(vertex_id_H, *w_it);
          model &= -theta[e][w] | -tw[w][Ti];
        }
      }
    }
  }

  //eq (10)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (auto e_it = es.first; e_it != es.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      auto v1 = get(vertex_id_G, source(*e_it, G.getGraph()));
      auto v2 = get(vertex_id_G, target(*e_it, G.getGraph()));
      for (size_t Si = 0; Si < H.getTransitionIndices(*w_it).size(); Si++) {
        auto S_ind = H.getTransitionIndices(*w_it)[Si];
        auto f1 = get(edge_id_H, H.getTransitions()[S_ind].getE1());
        auto f2 = get(edge_id_H, H.getTransitions()[S_ind].getE2());
        model &= (sw[w][Si] & (kappa[e][f1] | kappa[e][f2])) >> (cv[v1][w][0] | cv[v2][w][0]);
      }
    }
  }

  //eq (11)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (size_t Si = 0; Si < H.getTransitionIndices(*w_it).size(); Si++) {
      auto S_ind = H.getTransitionIndices(*w_it)[Si];
      auto out = out_edges(w, H.getGraph());
      for (auto f_it = out.first; f_it != out.second; f_it++) {
        auto f = get(edge_id_H, *f_it);
        if (f != get(edge_id_H, H.getTransitions()[S_ind].getE1()) &&
            f != get(edge_id_H, H.getTransitions()[S_ind].getE2())) {
          //f is not in pi2(S)

          for (auto e_it = es.first; e_it != es.second; e_it++) {
            auto e = get(edge_id_G, *e_it);
            auto v1 = source(*e_it, G.getGraph());
            auto v2 = target(*e_it, G.getGraph());
            model &= ((sw[w][Si] & kappa[e][f]) >> (cv[v1][w][1] | cv[v2][w][1]));
          }
        }
      }
    }
  }

  //eq (12)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (size_t Si = 0; Si < H.getTransitionIndices(*w_it).size(); Si++) {
      auto S_ind = H.getTransitionIndices(*w_it)[Si];
      auto f1 = get(edge_id_H, H.getTransitions()[S_ind].getE1());
      auto f2 = get(edge_id_H, H.getTransitions()[S_ind].getE2());
      for (auto v_it = vs.first; v_it != vs.second; v_it++) {
        auto v = get(vertex_id_G, *v_it);
        auto no_outgoing_arc = satLib::Formula::getTrue();
        auto aout = out_edges(v, simple);
        for (auto a_it = aout.first; a_it != aout.second; a_it++) {
          auto a = get(arc_id, *a_it);
          no_outgoing_arc &= -ca[a][w][0];
        }
        auto v_not_in_theta_preimage = satLib::Formula::getTrue();
        auto out = out_edges(v, G.getGraph());
        for (auto e_it = out.first; e_it != out.second; e_it++) {
          auto e = get(edge_id_G, *e_it);
          v_not_in_theta_preimage &= -theta[e][w];
        }
        auto e_v_cap_kappa_preimage_not_empty = satLib::Formula::getFalse();
        for (auto e_it = out.first; e_it != out.second; e_it++) {
          auto e = get(edge_id_G, *e_it);
          e_v_cap_kappa_preimage_not_empty |= kappa[e][f1] | kappa[e][f2];
        }
        model &= (sw[w][Si] & cv[v][w][0] & no_outgoing_arc & v_not_in_theta_preimage)
            >> (e_v_cap_kappa_preimage_not_empty | cv[v][w][1]);
      }
    }
  }

  //eq (13)
  for (size_t Ti = 0; Ti < G.getTransitions().size(); Ti++) {
    auto v = G.getTransitions()[Ti].getV();
    auto v11 = get(vertex_id_G, source(G.getTransitions()[Ti].getE1(), G.getGraph()));
    auto v12 = get(vertex_id_G, target(G.getTransitions()[Ti].getE1(), G.getGraph()));
    auto v21 = get(vertex_id_G, source(G.getTransitions()[Ti].getE2(), G.getGraph()));
    auto v22 = get(vertex_id_G, target(G.getTransitions()[Ti].getE2(), G.getGraph()));
    auto aout = out_edges(v, simple);
    for (auto a_it = aout.first; a_it != aout.second; a_it++) {
      auto a = get(arc_id, *a_it);
      auto v2 = get(sv_id, target(*a_it, simple));
      if (((v2 != v11 && v != v11) || (v2 != v12 && v != v12)) &&
          ((v2 != v21 && v != v21) || (v2 != v22 && v != v22))) {
        for (auto w_it = ws.first; w_it != ws.second; w_it++) {
          auto w = get(vertex_id_H, *w_it);
          model &= -tw[w][Ti] | -ca[a][w][0];
        }
      }
    }
  }

  //eq (14)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (auto e_it = es.first; e_it != es.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      auto v1 = source(*e_it, G.getGraph());
      auto v2 = target(*e_it, G.getGraph());
      model &= theta[e][w] >> (cv[v1][w][0] & cv[v2][w][0]);
    }
  }

  //eq (15)
  for (auto w_it = ws.first; w_it != ws.second; w_it++) {
    auto w = get(vertex_id_H, *w_it);
    for (auto e_it = es.first; e_it != es.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      auto v1 = source(*e_it, G.getGraph());
      auto v2 = target(*e_it, G.getGraph());
      auto a1 = get(arc_id, boost::edge(v1, v2, simple).first);
      auto a2 = get(arc_id, boost::edge(v2, v1, simple).first);
      model &= theta[e][w] >> (-ca[a1][w][0] & -ca[a2][w][0]);
    }
  }

  //symmetry breaking
  if (sym1 || sym2) {
    auto autoH = getAutomorphismsOfTransitionedGraph(H, sym2);
    if (sym1) {
      //add symmetry breaking 1
      auto autoG = getAutomorphismsOfTransitionedGraph(G, false);
      Vertex w0 = 0;
      if (autoH.getStabilizerVertices().size() > 0) {
        w0 = autoH.getStabilizerVertices()[0];
      }

      auto orbIndicesG = getOrbIndizes(autoG, num_vertices(G.getGraph()));
      auto orbIndicesH = getOrbIndizes(autoH, num_vertices(H.getGraph()));
      for (auto v_it = vs.first; v_it != vs.second; v_it++) {
        auto v = get(vertex_id_G, *v_it);
        for (auto w: autoH.getOrbits()[orbIndicesH[w0]]) {
          if (w != w0) {
            auto& orbit = autoG.getOrbits()[orbIndicesG[v]];
            auto minorb = *std::min_element(orbit.begin(), orbit.end());
            auto implication = satLib::Formula::getFalse();
            for (size_t v2 = 0; v2 <= minorb && v2 < v; v2++) {
              implication |= iota[v2][w0];
            }
            model &= iota[v][w] >> implication;
            if (hasSymmetries != nullptr) {
              *hasSymmetries = false;
            }
          }
        }
      }
    }
    if (sym2) {
      //add symmetry breaking 2
      for (size_t i = 0; i < autoH.getStabilizerVertices().size(); i++) {
        auto w = autoH.getStabilizerVertices()[i];
        for (auto w2: autoH.getStabilizerOrbits()[i]) {
          if (w2 != w) {
            for (auto v_it = vs.first; v_it != vs.second; v_it++) {
              auto v = get(vertex_id_G, *v_it);
              auto implication = satLib::Formula::getFalse();
              for (size_t v2 = 0; v2 < v; v2++) {
                implication |= iota[v2][w];
              }
              model &= iota[v][w2] >> implication;
              if (hasSymmetries != nullptr) {
                *hasSymmetries = false;
              }
            }
          }
        }
      }
    }
  }


  //DEBUG fix variables
  /*model &= iota[0][0] & iota[3][1] & iota[4][2] & iota[1][3] & iota[5][3] & iota[7][3] & iota[2][4] & iota[6][4];
  model &= kappa[1][0] & kappa[4][1] & kappa[13][2] & kappa[0][3] & kappa[6][4] & kappa[3][5] & kappa[2][6] &
      kappa[7][7] & kappa[5][8] & kappa[12][9];
  model &= tw[0][0] & tw[1][7] & tw[2][9] & tw[3][2] & tw[4][5];
  model &= sw[0][1] & sw[1][0] & sw[2][1] & sw[3][1] & sw[4][1];
  model &= ca[17][3][0];
  for (auto x: theta) {
    for (auto v: x) {
      model &= -v;
    }
  }
  print_edges(G.getGraph());
  print_edges(H.getGraph());

  for (size_t i = 0; i < G.getTransitions().size(); i++) {
    auto v = get(vertex_id_G, G.getTransitions()[i].getV());
    auto e1 = get(edge_id_G, G.getTransitions()[i].getE1());
    auto e2 = get(edge_id_G, G.getTransitions()[i].getE2());
    std::cout << i << ": (" << v << "," << e1 << "," << e2 << ")" << std::endl;
  }

  for (auto w_it = ws.first; w_it != ws.second; w_it ++) {
    auto w = get(vertex_id_H, *w_it);
    for (size_t i = 0; i < H.getTransitionIndices(*w_it).size(); i++) {
      auto transition = H.getTransitions()[H.getTransitionIndices(*w_it)[i]];
      auto f1 = get(edge_id_H, transition.getE1());
      auto f2 = get(edge_id_H, transition.getE2());
      std::cout << w << ": " << i << ": (" << f1 << "," << f2 << ")" << std::endl;
    }
  }

  for (auto a_it = as.first; a_it != as.second; a_it++) {
    auto a = get(arc_id, *a_it);
    auto v1 = get(sv_id, source(*a_it, simple));
    auto v2 = get(sv_id, target(*a_it, simple));
    std::cout << a << ": (" << v1 << "," << v2 << ")" << std::endl;
  }*/


  //solver.toDimacs("before");
  modellingTime = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;
  c_start = std::clock();
  auto cnf = model.convertToCNFAlgebraically();
  //std::cout << std::distance(cnf.getClauses().begin(), cnf.getClauses().end()) << ":";
  bool res = satLib::Glucose::solve(cnf);
  //std::cout << res << std::endl;
  solvingTime = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;

  if (!res && unsatFile != "") {
    //store unsat file
    cnf.print(unsatFile);
  }

  //solver.toDimacs("after");
  //std::cout << res << std::endl;
  //boost::print_edges2(G, vertex_id_G, edge_id_G);
  //boost::print_edges2(H, vertex_id_H, edge_id_H);
  /*if (!res) {
    boost::print_graph(G);
  } else {
    print(solver, vertex_map);
    print(solver, edge_map);
    print(solver, tree_arcs);
    print(solver, tree_roots);
    print(solver, closure);
  }*/
  //exit(0);
  return res;
}


#endif //PPM_TESTING_SUDMINORSOLVERSAT_H
