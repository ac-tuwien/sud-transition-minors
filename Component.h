//
// Created by benedikt on 10/23/18.
//

#ifndef PPM_TESTING_COMPONENT_H
#define PPM_TESTING_COMPONENT_H

#include <ostream>

enum ComponentType {
    edge, claw
};

template<class Vertex, class Edge>
class Component {
  public:
    explicit Component(const Edge& e);

    explicit Component(Vertex v);

    ComponentType getType() const {
      return type;
    }

    Vertex getV() const {
      return v;
    }

    Edge getE() const {
      return e;
    }

    template<class V, class E>
    friend std::ostream& operator<<(std::ostream &os, const Component<V, E>& o);

  private:
    ComponentType type;
    union {
        Vertex v;
        Edge e;
    };
};

template<class Vertex, class Edge>
Component<Vertex, Edge>::Component(Vertex v) : type(claw), v(v) {}

template<class Vertex, class Edge>
Component<Vertex, Edge>::Component(const Edge& e) : type(edge), e(e) {}

template<class Vertex, class Edge>
std::ostream& operator<<(std::ostream &os, const Component<Vertex, Edge>& o) {
  switch (o.getType()) {
    case edge: {
      os << "E" << o.getE();
      break;
    }
    case claw: {
      os << "C(" << o.getV() << ")";
      break;
    }
  }
  return os;
}

#endif //PPM_TESTING_COMPONENT_H
