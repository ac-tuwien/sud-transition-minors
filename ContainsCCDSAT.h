//
// Created by benedikt on 10/24/18.
//

#ifndef PPM_TESTING_CONTAINSCCD_H
#define PPM_TESTING_CONTAINSCCD_H

#include "simp/SimpSolver.h"
#include <vector>
#include "boost/graph/graph_traits.hpp"
#include <boost/functional/hash.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/named_graph.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include "TransitionedGraph.h"
#include <boost/graph/directed_graph.hpp>
#include "satLib/Formula.h"
#include "satLib/helpers.h"
#include "satLib/GlucoseConnector.h"

template<class Graph>
bool
containsCCD(TransitionedGraph<Graph> G, double& modellingTime, double& solvingTime) {
  std::clock_t c_start = std::clock();

  auto model = satLib::Formula::getTrue();

  /*
   * Variables
   */
  size_t maxNumberOfCycles = num_edges(G.getGraph()) / 2;

  auto var = satLib::createFunction(num_edges(G.getGraph()), maxNumberOfCycles, model, "C");

  auto vs = vertices(G.getGraph());
  auto edge_id = boost::get(boost::edge_index, G.getGraph());
  for (auto v_it = vs.first; v_it != vs.second; v_it++) {
    for (size_t i = 0; i < maxNumberOfCycles; i++) {
      //the cycle has at most 2 edges at this vertex
      auto out = boost::out_edges(*v_it, G.getGraph());
      for (auto e_it = out.first; e_it != out.second; e_it++) {
        auto e1 = get(edge_id, *e_it);
        auto e_it2 = e_it;
        e_it2++;
        for (; e_it2 != out.second; e_it2++) {
          auto e2 = get(edge_id, *e_it2);
          auto e_it3 = e_it2;
          e_it3++;
          for (; e_it3 != out.second; e_it3++) {
            auto e3 = get(edge_id, *e_it3);
            model &= -var[e1][i] | -var[e2][i] | -var[e3][i];
          }
        }
      }

      //if one edge is part of a cycle this implies at least one other edge, not in a transition is part of the cycle
      for (auto e_it = out.first; e_it != out.second; e_it++) {
        auto e1 = get(edge_id, *e_it);
        auto at_least_one_other = satLib::Formula::getFalse();
        for (auto e_it2 = out.first; e_it2 != out.second; e_it2++) {
          auto e2 = get(edge_id, *e_it2);
          if (e2 != e1) {
            //check if no transaction contains those two edges
            bool isConnected = false;
            for (auto index: G.getTransitionIndices(*v_it)) {
              auto transaction = G.getTransitions()[index];
              auto f1 = get(edge_id, transaction.getE1());
              auto f2 = get(edge_id, transaction.getE2());
              if ((e1 == f1 && e2 == f2) || (e1 == f2 && e2 == f1)) {
                isConnected = true;
                break;
              }
            }
            if (!isConnected) {
              at_least_one_other |= var[e2][i];
            }
          }
        }
        model &= var[e1][i] >> at_least_one_other;
      }
    }
  }

  //symmetry breaking var[e][i+1] -> Or_{f < e} var[f][i] for all i < maxNumberOfCycles - 1
  for (size_t i = 0; i < maxNumberOfCycles - 1; i++) {
    for (size_t e = 0; e < num_edges(G.getGraph()); e++) {
      auto smaller = satLib::Formula::getFalse();
      for (size_t f = 0; f < e; f++) {
        smaller |= var[f][i];
      }
      model &= var[e][i + 1] >> smaller;
    }
  }

  modellingTime = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;
  c_start = std::clock();
  auto cnf = model.convertToCNFAlgebraically();
  bool res = satLib::Glucose::solve(cnf);
  solvingTime = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;

  return res;
}


#endif //PPM_TESTING_CONTAINSCCD_H
