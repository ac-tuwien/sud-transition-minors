//
// Created by benedikt on 11/22/18.
//

#ifndef PPM_TESTING_CONTAINSSUDMINORMIP_H
#define PPM_TESTING_CONTAINSSUDMINORMIP_H

#include "TransitionedGraph.h"
#include <ctime>
#include <boost/graph/graph_traits.hpp>
#include <boost/functional/hash.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/named_graph.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <gurobi_c++.h>

template<class Graph>
void print_edges(Graph G) {
  auto es = edges(G);
  auto edge_id = boost::get(boost::edge_index, G);
  auto vertex_id = boost::get(boost::vertex_index, G);
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto e = get(edge_id, *e_it);
    auto v = get(vertex_id, source(*e_it, G));
    auto w = get(vertex_id, target(*e_it, G));
    std::cout << e << ": (" << v << "," << w << ")" << std::endl;
  }
}

class TimeLimitReachedError : public std::runtime_error {
  public:
    TimeLimitReachedError() : std::runtime_error("The time limit was reached") {}
};

std::vector<std::vector<GRBVar>>
createRelation(GRBModel& model, size_t domainSize, size_t imageSize, const std::string& relationName,
               bool isFunction = true) {
  std::vector<std::vector<GRBVar>> res(domainSize);
  for (size_t i = 0; i < domainSize; i++) {
    res[i].reserve(imageSize);
    for (size_t j = 0; j < imageSize; j++) {
      std::string name;
      if (isFunction) {
        name = relationName + "(" + std::to_string(i) + ")=" + std::to_string(j);
      } else {
        name = std::to_string(i) + relationName + std::to_string(j);
      }
      res[i].push_back(model.addVar(0.0, 1.0, 0.0, GRB_BINARY, name));
    }
  }
  return res;
}

std::vector<GRBVar>
createVector(GRBModel& model, size_t size, const std::string& name, double lb = 0.0, double ub = 1.0,
             char vtype = GRB_BINARY) {
  std::vector<GRBVar> res;
  res.reserve(size);
  for (size_t i = 0; i < size; i++) {
    res.push_back(model.addVar(lb, ub, 0.0, vtype, name + "_" + std::to_string(i)));
  }
  return res;
}

std::vector<std::vector<std::vector<GRBVar>>>
createThreeDim(GRBModel& model, size_t size1, size_t size2, size_t size3, const std::string& name, double lb = 0.0,
               double ub = 1.0, char vtype = GRB_BINARY) {
  std::vector<std::vector<std::vector<GRBVar>>> res(size1);
  for (size_t i = 0; i < size1; i++) {
    res[i] = std::vector<std::vector<GRBVar>>(size2);
    for (size_t j = 0; j < size2; j++) {
      res[i][j] = std::vector<GRBVar>();
      res[i][j].reserve(size3);
      for (size_t k = 0; k < size3; k++) {
        res[i][j].push_back(model.addVar(lb, ub, 0.0, vtype,
                                         name + "_" + std::to_string(i) + "_" + std::to_string(j) + "_" +
                                         std::to_string(k)));
      }
    }
  }
  return res;
}

GRBLinExpr sumLeft(const std::vector<std::vector<GRBVar>>& v, size_t i) {
  GRBLinExpr res;
  for (size_t j = 0; j < v.size(); j++) {
    res += v[j][i];
  }
  return res;
}

GRBLinExpr sum(const std::vector<GRBVar>& v) {
  GRBLinExpr res;
  for (size_t j = 0; j < v.size(); j++) {
    res += v[j];
  }
  return res;
}

void printVariables(GRBVar var) {
  if (var.get(GRB_DoubleAttr_X)) {
    std::cout << var.get(GRB_StringAttr_VarName) << std::endl;
  }
}

template<typename T> void printVariables(std::vector<T> vars) {
  for (auto var: vars) {
    printVariables(var);
  }
}

template<class Graph>
bool containsSUDMinorMIP(const TransitionedGraph<Graph>& G, const TransitionedGraph<Graph>& H, double& modellingTime,
                         double& solvingTime, bool sym1, bool sym2, double timelimit) {
  std::clock_t c_start = std::clock();

  typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
  typedef boost::property<boost::edge_index_t, std::size_t> DEdgeProperty;
  typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, boost::no_property, DEdgeProperty> DGraph;
  typedef typename boost::graph_traits<DGraph>::vertex_descriptor DVertex;
  DGraph simple;

  std::vector<DVertex> simple_vertices;
  simple_vertices.reserve(num_vertices(G.getGraph()));
  for (size_t i = 0; i < num_vertices(G.getGraph()); i++) {
    simple_vertices.push_back(add_vertex(simple));
  }
  auto es = edges(G.getGraph());
  size_t id = 0;
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto v1 = source(*e_it, G.getGraph());
    auto v2 = target(*e_it, G.getGraph());
    if (!boost::edge(v1, v2, simple).second) {
      add_edge(v1, v2, id++, simple);
      add_edge(v2, v1, id++, simple);
    }
  }

  try {
    GRBEnv env = GRBEnv();

    GRBModel model = GRBModel(env);

    size_t max_tree = num_vertices(simple) - num_vertices(H.getGraph()) + 1;
    // Create variables
    auto xs = createRelation(model, num_vertices(G.getGraph()), num_vertices(H.getGraph()), "x");
    auto ys = createRelation(model, num_edges(G.getGraph()), num_edges(H.getGraph()), "y");
    auto zs = createRelation(model, num_edges(G.getGraph()), num_vertices(H.getGraph()), "z");
    auto as_ = createRelation(model, G.getTransitions().size(), num_vertices(H.getGraph()), "a");
    auto bs = createVector(model, H.getTransitions().size(), "b");
    auto hs = createThreeDim(model, num_vertices(G.getGraph()), 2, num_vertices(H.getGraph()), "h");
    auto ts = createThreeDim(model, num_edges(simple), 2, num_vertices(H.getGraph()), "t");
    auto us = createVector(model, num_vertices(G.getGraph()), "u", 0.0, max_tree - 1, GRB_CONTINUOUS);

    // Basic structures constraints
    for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
      model.addConstr(sum(xs[v]) <= 1, "vmap_partial_mapping_" + std::to_string(v));
    }
    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      model.addConstr(sumLeft(xs, w) >= 1, "vmap_surjective_" + std::to_string(w));
    }

    for (size_t e = 0; e < num_edges(G.getGraph()); e++) {
      model.addConstr(sum(ys[e]) <= 1, "emap_partial_mapping_" + std::to_string(e));
    }
    for (size_t f = 0; f < num_edges(H.getGraph()); f++) {
      model.addConstr(sumLeft(ys, f) == 1, "emap_injective_surjective_" + std::to_string(f));
    }

    for (size_t e = 0; e < num_edges(G.getGraph()); e++) {
      model.addConstr(sum(zs[e]) <= 1, "theta_partial_mapping_" + std::to_string(e));
    }
    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      model.addConstr(sumLeft(zs, w) <= 1, "theta_injective_" + std::to_string(w));
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      model.addConstr(sumLeft(as_, w) == 1, "T_" + std::to_string(w) + "_exists");
    }
    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      GRBLinExpr sum;
      for (auto i: H.getTransitionIndices(w)) {
        sum += bs[i];
      }
      model.addConstr(sum == 1, "S_" + std::to_string(w) + "_exists");
    }

    auto as = edges(simple);
    auto arc_id = boost::get(boost::edge_index, simple);
    for (auto a_it = as.first; a_it != as.second; a_it++) {
      auto a = get(arc_id, *a_it);
      if (a % 2 == 0) {
        auto v1 = source(*a_it, simple);
        auto v2 = target(*a_it, simple);
        for (size_t i = 0; i < 2; i++) {
          for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
            model.addConstr(ts[a][i][w] + ts[a + 1][i][w] <= hs[v1][i][w],
                            "ts_hs_coupling_" + std::to_string(a) + "_" + std::to_string(i) + "_" + std::to_string(w) +
                            "_" + std::to_string(v1));
            model.addConstr(ts[a][i][w] + ts[a + 1][i][w] <= hs[v2][i][w],
                            "ts_hs_coupling_" + std::to_string(a) + "_" + std::to_string(i) + "_" + std::to_string(w) +
                            "_" + std::to_string(v2));
          }
        }
      }
    }

    for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
      for (size_t i = 0; i < 2; i++) {
        for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
          GRBLinExpr incoming;
          auto inc = in_edges(v, simple);
          for (auto a_it = inc.first; a_it != inc.second; a_it++) {
            incoming += ts[get(arc_id, *a_it)][i][w];
          }
          GRBLinExpr vTransitions;
          for (auto T: G.getTransitionIndices(v)) {
            vTransitions += as_[T][w];
          }
          model.addConstr(incoming == hs[v][i][w] - vTransitions,
                          "one_ingoing_arc_" + std::to_string(v) + "_" + std::to_string(i) + "_" + std::to_string(w));
        }
      }
    }

    for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
      GRBLinExpr isRoot;
      for (auto T: G.getTransitionIndices(v)) {
        for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
          isRoot += as_[T][w];
        }
      }
      model.addConstr(us[v] <= (max_tree - 1) * (1 - isRoot), "mtz_root_" + std::to_string(v));
    }

    for (auto a_it = as.first; a_it != as.second; a_it++) {
      auto a = get(arc_id, *a_it);
      auto v1 = source(*a_it, simple);
      auto v2 = target(*a_it, simple);
      model.addConstr(us[v1] - us[v2] + 1 <= max_tree * (1 - sum(ts[a][0]) - sum(ts[a][1])),
                      "mtz_" + std::to_string(a));
    }

    //General Constraints
    auto es = edges(G.getGraph());
    auto edge_id_G = boost::get(boost::edge_index, G.getGraph());
    auto fs = edges(H.getGraph());
    auto edge_id_H = boost::get(boost::edge_index, H.getGraph());
    for (auto e_it = es.first; e_it != es.second; e_it++) {
      auto e = get(edge_id_G, *e_it);
      auto v1 = source(*e_it, G.getGraph());
      auto v2 = target(*e_it, G.getGraph());
      for (auto f_it = fs.first; f_it != fs.second; f_it++) {
        auto f = get(edge_id_H, *f_it);
        auto w1 = source(*f_it, H.getGraph());
        auto w2 = target(*f_it, H.getGraph());

        model.addConstr(ys[e][f] <= xs[v1][w1] + xs[v1][w2],
                        "edge_vertex_coupling1_" + std::to_string(e) + "_" + std::to_string(f) + "_" +
                        std::to_string(v1));
        model.addConstr(ys[e][f] <= xs[v2][w1] + xs[v2][w2],
                        "edge_vertex_coupling1_" + std::to_string(e) + "_" + std::to_string(f) + "_" +
                        std::to_string(v2));

        model.addConstr(ys[e][f] <= xs[v1][w1] + xs[v2][w1],
                        "edge_vertex_coupling2_" + std::to_string(e) + "_" + std::to_string(f) + "_" +
                        std::to_string(w1));
        model.addConstr(ys[e][f] <= xs[v1][w2] + xs[v2][w2],
                        "edge_vertex_coupling2_" + std::to_string(e) + "_" + std::to_string(f) + "_" +
                        std::to_string(w2));
      }
    }

    for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
      for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
        GRBLinExpr isRoot;
        for (auto T: G.getTransitionIndices(v)) {
          isRoot += as_[T][w];
        }
        model.addConstr(hs[v][0][w] + hs[v][1][w] == xs[v][w] + isRoot,
                        "tree_vmap_coupling_" + std::to_string(v) + "_" + std::to_string(w));
        model.addConstr(isRoot <= xs[v][w], "Tw_vmap_coupling_" + std::to_string(v) + "_" + std::to_string(w));
      }
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      for (auto S: H.getTransitionIndices(w)) {
        for (size_t T = 0; T < G.getTransitions().size(); T++) {
          auto T_edges = {G.getTransitions()[T].getE1(), G.getTransitions()[T].getE2()};
          for (auto e_d: T_edges) {
            auto e = get(edge_id_G, e_d);
            auto v1 = source(e_d, G.getGraph());
            auto v2 = target(e_d, G.getGraph());
            auto a1 = get(arc_id, boost::edge(v1, v2, simple).first);
            auto a2 = get(arc_id, boost::edge(v2, v1, simple).first);
            model.addConstr(as_[T][w] + bs[S] - 1 <= ys[e][get(edge_id_H, H.getTransitions()[S].getE1())] +
                                                     ys[e][get(edge_id_H, H.getTransitions()[S].getE2())] + zs[e][w] +
                                                     ts[a1][0][w] + ts[a2][0][w],
                            "transition_edges_in_G_subset_" + std::to_string(T) + "_" + std::to_string(w) + "_" +
                            std::to_string(S) + "_" + std::to_string(e));
          }
        }
      }
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      for (auto S: H.getTransitionIndices(w)) {
        for (size_t T = 0; T < G.getTransitions().size(); T++) {
          auto S_edges = {H.getTransitions()[S].getE1(), H.getTransitions()[S].getE2()};
          for (auto f_d: S_edges) {
            auto f = get(edge_id_H, f_d);
            auto v = G.getTransitions()[T].getV();
            size_t T_edge_ids[2] = {get(edge_id_G, G.getTransitions()[T].getE1()),
                                    get(edge_id_G, G.getTransitions()[T].getE2())};
            GRBLinExpr nonTEdges;
            auto v_es = out_edges(v, G.getGraph());
            for (auto e_it = v_es.first; e_it != v_es.second; e_it++) {
              auto e = get(edge_id_G, *e_it);
              if (e != T_edge_ids[0] && e != T_edge_ids[1]) {
                nonTEdges += ys[e][f];
              }
            }
            model.addConstr(as_[T][w] + bs[S] - 1 <= 1 - nonTEdges,
                            "G_edges_transition_subset_" + std::to_string(T) + "_" + std::to_string(w) + "_" +
                            std::to_string(S) + "_" + std::to_string(f));
          }
        }
      }
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      for (size_t T = 0; T < G.getTransitions().size(); T++) {
        size_t T_edge_ids[2] = {get(edge_id_G, G.getTransitions()[T].getE1()),
                                get(edge_id_G, G.getTransitions()[T].getE2())};
        GRBLinExpr nonTEdges;
        for (auto e_it = es.first; e_it != es.second; e_it++) {
          auto e = get(edge_id_G, *e_it);
          if (e != T_edge_ids[0] && e != T_edge_ids[1]) {
            nonTEdges += zs[e][w];
          }
        }
        model.addConstr(as_[T][w] <= 1 - nonTEdges,
                        "theta_edges_subset_transition_" + std::to_string(T) + "_" + std::to_string(w));
      }
    }

    for (size_t S = 0; S < H.getTransitions().size(); S++) {
      auto w = H.getTransitions()[S].getV();
      for (auto e_it = es.first; e_it != es.second; e_it++) {
        auto e = get(edge_id_G, *e_it);
        auto v1 = source(*e_it, G.getGraph());
        auto v2 = target(*e_it, G.getGraph());
        model.addConstr(bs[S] + ys[e][get(edge_id_H, H.getTransitions()[S].getE1())] +
                        ys[e][get(edge_id_H, H.getTransitions()[S].getE2())] <= 1 + hs[v1][0][w] + hs[v2][0][w],
                        "transition_edges_incident_to_vertex_in_C1_" + std::to_string(S) + "_" + std::to_string(e));
      }
    }

    for (size_t S = 0; S < H.getTransitions().size(); S++) {
      auto w = H.getTransitions()[S].getV();
      size_t S_edge_ids[2] = {get(edge_id_H, H.getTransitions()[S].getE1()),
                              get(edge_id_H, H.getTransitions()[S].getE2())};
      auto w_fs = out_edges(w, H.getGraph());
      for (auto e_it = es.first; e_it != es.second; e_it++) {
        auto e = get(edge_id_G, *e_it);
        auto v1 = source(*e_it, G.getGraph());
        auto v2 = target(*e_it, G.getGraph());
        GRBLinExpr non_S_edges;
        for (auto f_it = w_fs.first; f_it != w_fs.second; f_it++) {
          auto f = get(edge_id_H, *f_it);
          if (f != S_edge_ids[0] && f != S_edge_ids[1]) {
            non_S_edges += ys[e][f];
          }
        }
        model.addConstr(bs[S] + non_S_edges <= 1 + hs[v1][1][w] + hs[v2][1][w],
                        "non_transition_edges_incident_to_vertex_in_C2_" + std::to_string(S) + "_" + std::to_string(e));
      }
    }

    for (size_t S = 0; S < H.getTransitions().size(); S++) {
      auto w = H.getTransitions()[S].getV();
      for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
        GRBLinExpr asSum;
        for (auto T: G.getTransitionIndices(v)) {
          asSum += as_[T][w];
        }

        GRBLinExpr tsSum;
        auto vaout = out_edges(v, simple);
        for (auto a_it = vaout.first; a_it != vaout.second; a_it++) {
          tsSum += ts[get(arc_id, *a_it)][0][w];
        }
        auto vain = in_edges(v, simple);
        for (auto a_it = vain.first; a_it != vain.second; a_it++) {
          tsSum += ts[get(arc_id, *a_it)][0][w];
        }

        GRBLinExpr zsSum;
        GRBLinExpr ysSum;
        auto ves = out_edges(v, G.getGraph());
        for (auto e_it = ves.first; e_it != ves.second; e_it++) {
          auto e = get(edge_id_G, *e_it);
          zsSum += zs[e][w];
          ysSum += ys[e][get(edge_id_H, H.getTransitions()[S].getE1())] +
                   ys[e][get(edge_id_H, H.getTransitions()[S].getE2())];
        }
        model.addConstr(bs[S] + hs[v][0][w] - 1 - asSum <= tsSum / 2 + zsSum + ysSum,
                        "leaf_vertices_in_C1_tree_connected_to_kappa_" + std::to_string(v) + "_" + std::to_string(S));
      }
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      for (size_t T = 0; T < G.getTransitions().size(); T++) {
        auto v0 = G.getTransitions()[T].getV();
        std::unordered_set<Vertex> sig_g_pi2;
        sig_g_pi2.insert(source(G.getTransitions()[T].getE1(), G.getGraph()));
        sig_g_pi2.insert(target(G.getTransitions()[T].getE1(), G.getGraph()));
        sig_g_pi2.insert(source(G.getTransitions()[T].getE2(), G.getGraph()));
        sig_g_pi2.insert(target(G.getTransitions()[T].getE2(), G.getGraph()));
        auto vns = adjacent_vertices(v0, G.getGraph());
        for (auto v_it = vns.first; v_it != vns.second; v_it++) {
          auto v = *v_it;
          if (sig_g_pi2.count(v) == 0) {
            auto a1 = get(arc_id, boost::edge(v0, v, simple).first);
            auto a2 = get(arc_id, boost::edge(v, v0, simple).first);
            model.addConstr(as_[T][w] <= 1 - ts[a1][0][w] - ts[a2][0][w],
                            "only_transition_edges_in_C1_incident_to_transition_vertex_" + std::to_string(T) + "_" +
                            std::to_string(w) + "_" + std::to_string(v));
          }
        }
      }
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      for (auto e_it = es.first; e_it != es.second; e_it++) {
        auto e = get(edge_id_G, *e_it);
        model.addConstr(2 * zs[e][w] <= hs[source(*e_it, G.getGraph())][0][w] + hs[target(*e_it, G.getGraph())][0][w],
                        "theta_edge_vertices_in_C1_" + std::to_string(e) + "_" + std::to_string(w));
      }
    }

    for (size_t w = 0; w < num_vertices(H.getGraph()); w++) {
      for (auto e_it = es.first; e_it != es.second; e_it++) {
        auto e = get(edge_id_G, *e_it);
        auto v1 = source(*e_it, G.getGraph());
        auto v2 = target(*e_it, G.getGraph());
        auto a1 = get(arc_id, boost::edge(v1, v2, simple).first);
        auto a2 = get(arc_id, boost::edge(v2, v1, simple).first);
        model.addConstr(zs[e][w] <= 1 - ts[a1][0][w] - ts[a2][0][w],
                        "theta_edge_not_in_C1_" + std::to_string(e) + "_" + std::to_string(w));
      }
    }

    //symmetry breaking
    if (sym1 || sym2) {
      auto autoH = getAutomorphismsOfTransitionedGraph(H, sym2);
      if (sym1) {
        //add symmetry breaking 1
        auto autoG = getAutomorphismsOfTransitionedGraph(G, false);
        Vertex w0 = 0;
        if (autoH.getStabilizerVertices().size() > 0) {
          w0 = autoH.getStabilizerVertices()[0];
        }

        auto orbIndicesG = getOrbIndizes(autoG, num_vertices(G.getGraph()));
        auto orbIndicesH = getOrbIndizes(autoH, num_vertices(H.getGraph()));
        for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
          for (auto w: autoH.getOrbits()[orbIndicesH[w0]]) {
            auto& orbit = autoG.getOrbits()[orbIndicesG[v]];
            auto minorb = *std::min_element(orbit.begin(), orbit.end());
            assert(v >= minorb);
            GRBLinExpr sumSmaller = 0;
            for (size_t v2 = 0; v2 <= minorb; v2++) {
              sumSmaller += xs[v2][w0];
            }
            model.addConstr(xs[v][w] <= sumSmaller,
                            "symmetry_breaking_1_" + std::to_string(v) + "_" + std::to_string(w));
          }
        }
      }
      if (sym2) {
        //add symmetry breaking 2
        for (size_t i = 0; i < autoH.getStabilizerVertices().size(); i++) {
          auto w = autoH.getStabilizerVertices()[i];
          for (auto w2: autoH.getStabilizerOrbits()[i]) {
            if (w2 != w) {
              for (size_t v = 0; v < num_vertices(G.getGraph()); v++) {
                GRBLinExpr sumSmaller = 0;
                for (size_t v2 = 0; v2 < v; v2++) {
                  sumSmaller += xs[v2][w];
                }
                model.addConstr(xs[v][w2] <= sumSmaller,
                                "symmetry_breaking_2_" + std::to_string(v) + "_" + std::to_string(w));
              }
            }
          }
        }
      }
    }

    // Optimize model
    modellingTime = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;
    c_start = std::clock();

    if (timelimit > 0.0) {
      model.set(GRB_DoubleParam_TimeLimit, timelimit);
    }
    model.set(GRB_IntParam_Threads, 1);
    model.set(GRB_IntParam_LogToConsole, 0);
    model.set(GRB_StringParam_LogFile, "gurobi.log");
    //model.set(GRB_IntParam_OutputFlag, 0);
    //fix variables

    model.optimize();
    solvingTime = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;

    switch (model.get(GRB_IntAttr_Status)) {
    case GRB_OPTIMAL: {
      /*//print graph G
      print_edges(G.getGraph());
      print_edges(simple);
      for (size_t i = 0; i < G.getTransitions().size(); i++) {
        auto v = G.getTransitions()[i].getV();
        auto e1 = get(edge_id_G, G.getTransitions()[i].getE1());
        auto e2 = get(edge_id_G, G.getTransitions()[i].getE2());
        std::cout << i << ": (" << v << "," << e1 << "," << e2 << ")" << std::endl;
      }

      model.write("model.lp");

      //print variables
      printVariables(xs);
      printVariables(ys);
      printVariables(zs);
      printVariables(as_);
      printVariables(bs);
      printVariables(hs);
      printVariables(ts);*/

      return true;
    }
    case GRB_INFEASIBLE: {
      //model.computeIIS();
      //model.write("iis.ilp");
      return false;
    }
    case GRB_TIME_LIMIT: {
      throw TimeLimitReachedError();
    }
    default: {
      std::cerr << "Unexpected Gurobi Error!" << std::endl;
      exit(1);
    }
    }
  } catch (GRBException e) {
    std::cerr << "Error code = " << e.getErrorCode() << std::endl;
    std::cerr << e.getMessage() << std::endl;
    exit(1);
  }
}


#endif //PPM_TESTING_CONTAINSSUDMINORMIP_H
