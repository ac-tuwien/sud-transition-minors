//
// Created by benedikt on 10/23/18.
//

#ifndef PPM_TESTING_FINDPPM_H
#define PPM_TESTING_FINDPPM_H

#include "PerfectPseudoMatchingEnumerator.h"
#include <boost/graph/boyer_myrvold_planar_test.hpp>
#include <boost/graph/named_graph.hpp>
#include "ContainsMinorSAT.h"
#include "ContainsSUDMinorSAT.h"
#include "CPUTimes.h"
#include <ctime>
#include <vector>
#include <deque>
#include "ContainsCCDSAT.h"
#include <iostream>
#include <fstream>

class FindPPM {
  public:
    explicit FindPPM(CPUTimes* times, bool minimal = false, bool recheck_implications = false,
                     std::string badK5InstancesDir = "", bool sym1 = true, bool sym2 = true, bool mip = false,
                     bool listAllMatchings = false, std::string unsatDir = "");

    FindPPM(CPUTimes* times, bool minimal, bool recheck_implications, std::string badK5InstancesDir, bool sym1,
            bool sym2, bool mip, const std::vector<Property>& properties, bool listAllMatchings = false,
            std::string unsatDir = "");

    template<class Graph>
    std::deque<PerfectPseudoMatching<Graph>>
    findPPM(const Graph& graph, bool includePseudo = true, std::string filename_prefix = "");

  private:
    template<class Graph>
    void checkPPMs(std::deque<PerfectPseudoMatching<Graph>>&, std::string filename_prefix, bool checkPlanar = true);

    template<class Graph>
    bool checkPlanarity(PerfectPseudoMatching<Graph>& matching);

    template<class Graph>
    Graph getCompleteGraph(int n);

    CPUTimes* times;
    bool minimal;
    bool recheck_implications;
    std::vector<Property> properties;
    std::string badK5InstancesDir;
    bool sym1;
    bool sym2;
    bool mip;
    bool listAllMatchings;
    std::string unsatDir;
};

template<class Graph>
std::deque<PerfectPseudoMatching<Graph>>
FindPPM::findPPM(const Graph& graph, bool includePseudo, std::string filename_prefix) {
  PerfectPseudoMatchingEnumerator<Graph> enumerator(graph, includePseudo);
  if (minimal && !listAllMatchings && properties.size() > 0 && properties[0] == Planar) {
    auto matchings = enumerator.getAllOrFirstSatisfying([this](PerfectPseudoMatching<Graph>& m) -> bool {
      return checkPlanarity(m);
    });
    if (matchings.size() == 0 || matchings[matchings.size() - 1].getProperty(Planar) != True) {
      checkPPMs(matchings, filename_prefix, false);
    }
    return matchings;
  } else {
    auto matchings = enumerator.getAll();
    checkPPMs(matchings, filename_prefix);
    return matchings;
  }
}

template<class Graph>
bool FindPPM::checkPlanarity(PerfectPseudoMatching<Graph>& matching) {
  std::clock_t c_start = std::clock();
  bool planarizing = boost::boyer_myrvold_planarity_test(matching.getOrCreateContracted().getGraph());
  double time = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;
  times->planarity_times.push_back(time);
  if (planarizing) {
    times->Proving_Planarity_times.push_back(time);
  }
  matching.setProperty(Planar, planarizing);
  return planarizing;
}

template<class Graph>
void
FindPPM::checkPPMs(std::deque<PerfectPseudoMatching<Graph>>& matchings, std::string filename_prefix, bool checkPlanar) {
  TransitionedGraph<Graph> sudK5;
  sudK5.setGraph(getCompleteGraph<Graph>(5));
  auto complete = sudK5.getGraph();
  //add transitions
  auto vs = vertices(complete);
  auto vertex_id = boost::get(boost::vertex_index, complete);
  typedef typename TransitionedGraph<Graph>::Vertex Vertex;
  typedef typename TransitionedGraph<Graph>::Edge Edge;
  std::vector<Transition<Vertex, Edge>> transitions;
  transitions.reserve(10);
  for (auto v_it = vs.first; v_it != vs.second; v_it++) {
    auto v = get(vertex_id, *v_it);
    auto v1 = (v + 4) % 5;
    auto v2 = (v + 1) % 5;
    auto e1 = boost::edge(v, v1, complete).first;
    auto e2 = boost::edge(v, v2, complete).first;
    transitions.push_back(Transition<Vertex, Edge>(v, e1, e2));
    v1 = (v + 3) % 5;
    v2 = (v + 2) % 5;
    e1 = boost::edge(v, v1, complete).first;
    e2 = boost::edge(v, v2, complete).first;
    transitions.push_back(Transition<Vertex, Edge>(v, e1, e2));
  }

  sudK5.setTransitions(transitions);

  size_t bad_k5_minor_free_count = 0;
  for (auto& prop: properties) {
    for (size_t i = 0; i < matchings.size(); i++) {
      auto& matching = matchings[i];
      if (recheck_implications || matching.getProperty(prop) == Unknown) {
        switch (prop) {
        case Planar: {
          if (checkPlanar || matching.getProperty(Planar) == Unknown) {
            checkPlanarity(matching);
          }
          break;
        }
        case K5MinorFree: {
          double modellingTime;
          double solvingTime;
          bool k5minorFree = !containsMinor(matching.getOrCreateContracted().getGraph(), complete, modellingTime,
                                            solvingTime);
          times->K5_minor_modelling_times.push_back(modellingTime);
          times->K5_minor_solving_times.push_back(solvingTime);
          if (k5minorFree) {
            times->Proving_K5_minor_freeness_times.push_back(solvingTime);
          }
          matching.setProperty(K5MinorFree, k5minorFree);
          break;
        }
        case BadK5MinorFree: {
          double modellingTime;
          double solvingTime;
          //boost::print_graph(matching.getGraph());
          //std::cout << matching << std::endl;
          bool hasSymmetries;
          bool badK5minorFree = !containsSUDMinor(matching.getOrCreateContracted(), sudK5, modellingTime, solvingTime,
                                                  sym1, sym2, mip, 0.0, &hasSymmetries,
                                                  unsatDir + "/" +filename_prefix + "_" + std::to_string(i) + ".cnf");
          times->Bad_K5_minor_modelling_times.push_back(modellingTime);
          times->Bad_K5_minor_solving_times.push_back(solvingTime);
          if (badK5minorFree) {
            if (badK5InstancesDir != "" && filename_prefix != "") {
              //log instance
              auto g = matching.getGraph();
              std::string instance_name = filename_prefix;
              if (bad_k5_minor_free_count > 0) {
                instance_name += "_" + std::to_string(bad_k5_minor_free_count);
              }
              bad_k5_minor_free_count++;
              std::ofstream ofile;
              ofile.open(badK5InstancesDir + "/" + instance_name);
              matching.getOrCreateContracted().printTo(ofile, "G", "T");
              ofile << std::endl;
              sudK5.printTo(ofile, "H", "S");
              ofile << std::endl;
            }
            times->Proving_Bad_K5_minor_freeness_times.push_back(solvingTime);
          }
          matching.setProperty(BadK5MinorFree, badK5minorFree);
          break;
        }
        case ContainsCCD: {
          double modellingTime;
          double solvingTime;
          bool contains = containsCCD(matching.getOrCreateContracted(), modellingTime, solvingTime);
          times->CCD_modelling_times.push_back(modellingTime);
          times->CCD_solving_times.push_back(solvingTime);
          if (contains) {
            times->Proving_CCD_existence_times.push_back(solvingTime);
          }
          matching.setProperty(ContainsCCD, contains);
          break;
        }
        }
        if (minimal && matching.getProperty(prop) == True) {
          return;
        }
      }
    }
  }
}

FindPPM::FindPPM(CPUTimes* times, bool minimal, bool recheck_implications, std::string badK5InstancesDir, bool sym1,
                 bool sym2, bool mip, bool listAllMatchings, std::string unsatDir) : FindPPM(times, minimal,
                                                                                             recheck_implications,
                                                                                             badK5InstancesDir, sym1,
                                                                                             sym2, mip,
                                                                                             {Planar, K5MinorFree,
                                                                                              BadK5MinorFree,
                                                                                              ContainsCCD},
                                                                                             listAllMatchings,
                                                                                             unsatDir) {}

FindPPM::FindPPM(CPUTimes* times, bool minimal, bool recheck_implications, std::string badK5InstancesDir, bool sym1,
                 bool sym2, bool mip, const std::vector<Property>& properties, bool listAllMatchings,
                 std::string unsatDir) : times(times), minimal(minimal), recheck_implications(recheck_implications),
                                         properties(properties), badK5InstancesDir(badK5InstancesDir), sym1(sym1),
                                         sym2(sym2), mip(mip), listAllMatchings(listAllMatchings), unsatDir(unsatDir) {}

template<class Graph>
Graph FindPPM::getCompleteGraph(int n) {
  Graph g;
  std::size_t edge_index = 0;
  for (int i = 0; i < n; i++) {
    for (int j = i + 1; j < n; j++) {
      add_edge(i, j, edge_index++, g);
    }
  }
  return g;
}


#endif //PPM_TESTING_FINDPPM_H
