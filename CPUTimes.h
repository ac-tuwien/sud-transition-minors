//
// Created by benedikt on 10/31/18.
//

#ifndef PPM_TESTING_CPUTIMES_H
#define PPM_TESTING_CPUTIMES_H

#include <vector>
#include <ostream>
#include <string>
#include <numeric>
#include <algorithm>
#include <assert.h>

class CPUTimes {
  public:
    double total_run_time;
    std::vector<double> planarity_times;
    std::vector<double> K5_minor_modelling_times;
    std::vector<double> K5_minor_solving_times;
    std::vector<double> Bad_K5_minor_modelling_times;
    std::vector<double> Bad_K5_minor_solving_times;
    std::vector<double> CCD_modelling_times;
    std::vector<double> CCD_solving_times;
    std::vector<double> Proving_Planarity_times;
    std::vector<double> Proving_K5_minor_freeness_times;
    std::vector<double> Proving_Bad_K5_minor_freeness_times;
    std::vector<double> Proving_CCD_existence_times;
};

double median(std::vector<double> v) {
  assert(!v.empty());
  if (v.size() % 2 == 0) {
    const auto median_it1 = v.begin() + v.size() / 2 - 1;
    const auto median_it2 = v.begin() + v.size() / 2;

    std::nth_element(v.begin(), median_it1 , v.end());
    const auto e1 = *median_it1;

    std::nth_element(v.begin(), median_it2 , v.end());
    const auto e2 = *median_it2;

    return (e1 + e2) / 2;

  } else {
    const auto median_it = v.begin() + v.size() / 2;
    std::nth_element(v.begin(), median_it , v.end());
    return *median_it;
  }
}

void append_statistical(std::ostream &os, const std::vector<double>& values, const std::string& prefix) {
  os << std::endl << prefix << "count: " << values.size();
  if (values.empty()) {
    return;
  }
  os << std::endl << prefix << "min: " << *(std::min_element(values.begin(), values.end()));
  os << std::endl << prefix << "max: " << *(std::max_element(values.begin(), values.end()));
  os << std::endl << prefix << "sum: " << std::accumulate(values.begin(), values.end(), 0.0);
  os << std::endl << prefix << "median: " << median(values);
}

std::ostream& operator<<(std::ostream &os, const CPUTimes& o) {
  os << "Total run time: " << o.total_run_time;
  append_statistical(os, o.planarity_times, "Times for Checking Planarity ");
  append_statistical(os, o.Proving_Planarity_times, "Times for Proving Planarity ");
  append_statistical(os, o.K5_minor_modelling_times, "Times for Modelling K5-minors ");
  append_statistical(os, o.K5_minor_solving_times, "Times for Solving K5-minors ");
  append_statistical(os, o.Proving_K5_minor_freeness_times, "Times for Proving K5-minor-freeness ");
  append_statistical(os, o.Bad_K5_minor_modelling_times, "Times for Modelling Bad-K5-minors ");
  append_statistical(os, o.Bad_K5_minor_solving_times, "Times for Solving Bad-K5-minors ");
  append_statistical(os, o.Proving_Bad_K5_minor_freeness_times, "Times for Proving Bad-K5-minor-freeness ");
  append_statistical(os, o.CCD_modelling_times, "Times for Modelling CCD ");
  append_statistical(os, o.CCD_solving_times, "Times for Solving CCD ");
  append_statistical(os, o.Proving_CCD_existence_times, "Times for Proving CCD existence ");
  return os;
}

#endif //PPM_TESTING_CPUTIMES_H
