//
// Created by benedikt on 10/23/18.
//

#ifndef PPM_TESTING_GRAPHPARSER_H
#define PPM_TESTING_GRAPHPARSER_H

#include <vector>
#include <iostream>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/graph/named_graph.hpp>
#include "TransitionedGraph.h"

class GraphParser {
  public:
    template<class Graph>
    Graph parseGraph6(std::istream& graph6);

    template<class Graph>
    std::pair<const TransitionedGraph<Graph>*, const TransitionedGraph<Graph>*> parseSingleInstance(std::istream& input);

  private:
    template<class Graph>
    const TransitionedGraph<Graph>* parseTransitionedGraph(std::istream& input);
};

unsigned long getBinaryValue(std::string const& s, size_t left, size_t length) {
  unsigned long res = 0;
  for (size_t i = 0; i < length; i++) {
    res *= 64;
    assert(s[left + i] >= 63);
    res += (unsigned long) (s[left + i] - 63);
  }
  return res;
}

template<class Graph>
Graph GraphParser::parseGraph6(std::istream& graph6) {
  std::string line;
  getline(graph6, line);
  std::string prefix = ">>graph6<<";
  if (boost::starts_with(line, prefix)) {
    line = line.substr(prefix.length());
  }
  unsigned long n = 0;
  unsigned long i = 0;
  if (line[0] <= 125) {
    //graph size is between 0 and 62
    assert(line[0] >= 63);
    n = (unsigned long) (line[0] - 63);
    i = 1;
  } else if (line[1] <= 125) {
    n = getBinaryValue(line, 1, 3);
    i = 4;
  } else {
    n = getBinaryValue(line, 2, 6);
    i = 8;
  }
  Graph g(n);
  unsigned long left = 0;
  unsigned long right = 1;
  std::size_t edge_index = 0;
  for (; i < line.length(); i++) {
    char c = line[i];
    assert(c >= 63);
    std::bitset<6> set((unsigned int) (c - 63));
    for (int j = 5; j >= 0; j--) {
      if (right >= n) {
        assert(j < 5 && set[j] == 0);
      } else {
        if (set[j]) {
          add_edge(left, right, edge_index++, g);
        }
        left++;
        if (left == right) {
          left = 0;
          right++;
        }
      }
    }
  }
  return g;
}

template<class Graph>
std::pair<const TransitionedGraph<Graph>*, const TransitionedGraph<Graph>*> GraphParser::parseSingleInstance(std::istream& input) {
  std::string line;
  std::getline(input, line);
  assert(line == "#G");
  auto G = parseTransitionedGraph<Graph>(input);
  auto H = parseTransitionedGraph<Graph>(input);
  return std::make_pair(G, H);
}

std::istream& colon(std::istream& in) {
  std::istream::sentry cerberos(in);
  if (cerberos) {
    if (in.peek() == ':') {
      in.ignore();
    }
    else {
      in.setstate(std::ios_base::failbit);
    }
  }
  return in;
}

template<class Graph>
const TransitionedGraph<Graph>* GraphParser::parseTransitionedGraph(std::istream& input) {
  std::string line;
  bool transitionSystem = false;
  TransitionedGraph<Graph>* g = new TransitionedGraph<Graph>();
  typedef Transition<typename TransitionedGraph<Graph>::Vertex, typename TransitionedGraph<Graph>::Edge> Trans;
  std::vector<Trans> transitions;
  std::unordered_map<int, typename TransitionedGraph<Graph>::Edge> es;
  while (std::getline(input, line)) {
    if (boost::starts_with(line, "#")) {
      if (!transitionSystem) {
        transitionSystem = true;
        auto ep = edges(g->getGraph());
        auto edge_id = boost::get(boost::edge_index, g->getGraph());
        for (auto eit = ep.first; eit != ep.second; eit++) {
          es.insert(std::make_pair(boost::get(edge_id, *eit), *eit));
        }
      } else {
        break;
      }
    } else if (!transitionSystem) {
      std::istringstream iss(line);
      int ind, v1, v2;
      iss >> ind >> colon >> v1 >> v2;
      add_edge(v1, v2, ind, g->getGraph());
    } else {
      std::istringstream iss(line);
      int v, e1, e2;
      iss >> v >> e1 >> e2;
      transitions.push_back(Trans(v, es[e1], es[e2]));
    }
  }
  g->setTransitions(transitions);
  return g;
}

#endif //PPM_TESTING_GRAPHPARSER_H
