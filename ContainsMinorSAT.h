//
// Created by benedikt on 10/24/18.
//

#ifndef PPM_TESTING_K5MINORSOLVERSAT_H
#define PPM_TESTING_K5MINORSOLVERSAT_H

#include "simp/SimpSolver.h"
#include <vector>
#include "boost/graph/graph_traits.hpp"
#include <boost/functional/hash.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/named_graph.hpp>
#include <boost/graph/graph_concepts.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>

//TODO use satLib
typedef std::pair<Glucose::Var, std::string> Var;
typedef std::vector<std::vector<Var>> Relation;
typedef Glucose::SimpSolver SATSolver;

Relation createRelation(size_t domainSize, size_t imageSize, SATSolver& solver, const std::string& relationName,
                        bool isFunction = true) {
  Relation res(domainSize);
  for (size_t i = 0; i < domainSize; i++) {
    res[i].reserve(imageSize);
    for (size_t j = 0; j < imageSize; j++) {
      auto var = solver.newVar();
      std::string name;
      if (isFunction) {
        name = relationName + "(" + std::to_string(i) + ") = " + std::to_string(j);
      } else {
        name = std::to_string(i) + relationName + std::to_string(j);
      }
      res[i].push_back(std::make_pair(var, name));
    }
  }
  return res;
}

void partial(Relation& relation, SATSolver& solver) {
  for (auto& images: relation) {
    for (size_t j = 0; j < images.size(); j++) {
      for (size_t k = j + 1; k < images.size(); k++) {
        solver.addClause(Glucose::mkLit(images[j].first, true), Glucose::mkLit(images[k].first, true));
      }
    }
  }
}

Relation
createPartialFunction(size_t domainSize, size_t imageSize, SATSolver& solver, const std::string& relationName) {
  auto function = createRelation(domainSize, imageSize, solver, relationName, true);
  partial(function, solver);
  return function;
}

void injective(Relation& relation, SATSolver& solver) {
  for (size_t i = 0; i < relation.size(); i++) {
    for (size_t j = i + 1; j < relation.size(); j++) {
      for (size_t k = 0; k < relation[i].size(); k++) {
        solver.addClause(Glucose::mkLit(relation[i][k].first, true), Glucose::mkLit(relation[j][k].first, true));
      }
    }
  }
}

void surjective(Relation& relation, SATSolver& solver) {
  if (relation.empty()) {
    return;
  }
  for (size_t j = 0; j < relation[0].size(); j++) {
    Glucose::vec<Glucose::Lit> clause;
    for (auto& images: relation) {
      clause.push(Glucose::mkLit(images[j].first));
    }
    solver.addClause(clause);
  }

}

/*void print(SATSolver& solver, Relation relation) {
  for (auto& vs: relation) {
    for (auto& p: vs) {
      if (solver.modelValue(p.first) != l_False) {
        std::cout << p.second << std::endl;
      }
    }
  }
}*/

template<class Graph>
bool containsMinor(Graph G, Graph H, double& modellingTime, double& solvingTime) {
  std::clock_t c_start = std::clock();
  SATSolver solver;
  solver.verbosity = -100;

  typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;

  std::unordered_set<std::pair<Vertex, Vertex>, boost::hash<std::pair<Vertex, Vertex> > > simple_arcs_set;
  auto es = edges(G);
  for (auto e_it = es.first; e_it != es.second; e_it++) {

    simple_arcs_set.insert(std::make_pair(source(*e_it, G), target(*e_it, G)));
    simple_arcs_set.insert(std::make_pair(target(*e_it, G), source(*e_it, G)));
  }
  std::vector<std::pair<Vertex, Vertex>> simple_arcs(simple_arcs_set.begin(), simple_arcs_set.end());

  /*
   * Variables
   */
  auto vertex_map = createPartialFunction(num_vertices(G), num_vertices(H), solver, "v");
  surjective(vertex_map, solver);
  auto edge_map = createPartialFunction(num_edges(G), num_edges(H), solver, "e");
  injective(edge_map, solver);
  surjective(edge_map, solver);
  auto tree_arcs = createPartialFunction(simple_arcs.size(), num_vertices(H), solver, "a");
  auto tree_roots = createPartialFunction(num_vertices(G), num_vertices(H), solver, "r");
  injective(tree_roots, solver);
  surjective(tree_roots, solver);
  // transitive closure of tree arcs
  auto closure = createRelation(num_vertices(G), num_vertices(G), solver, "->", false);

  //couple vertex_map with edge_map
  auto fs = edges(H);
  auto edge_id_G = boost::get(boost::edge_index, G);
  auto vertex_id_G = boost::get(boost::vertex_index, G);
  auto edge_id_H = boost::get(boost::edge_index, H);
  auto vertex_id_H = boost::get(boost::vertex_index, H);
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto e_ind = get(edge_id_G, *e_it);
    Vertex v1 = boost::source(*e_it, G);
    auto v1_ind = get(vertex_id_G, v1);
    Vertex v2 = boost::target(*e_it, G);
    auto v2_ind = get(vertex_id_G, v2);
    for (auto f_it = fs.first; f_it != fs.second; f_it++) {
      auto f_ind = get(edge_id_H, *f_it);
      Vertex w1 = boost::source(*f_it, G);
      auto w1_ind = get(vertex_id_H, w1);
      Vertex w2 = boost::target(*f_it, G);
      auto w2_ind = get(vertex_id_H, w2);
      solver.addClause(Glucose::mkLit(edge_map[e_ind][f_ind].first, true),
                       Glucose::mkLit(vertex_map[v1_ind][w1_ind].first),
                       Glucose::mkLit(vertex_map[v1_ind][w2_ind].first));
      solver.addClause(Glucose::mkLit(edge_map[e_ind][f_ind].first, true),
                       Glucose::mkLit(vertex_map[v2_ind][w1_ind].first),
                       Glucose::mkLit(vertex_map[v2_ind][w2_ind].first));
      solver.addClause(Glucose::mkLit(edge_map[e_ind][f_ind].first, true),
                       Glucose::mkLit(vertex_map[v1_ind][w1_ind].first),
                       Glucose::mkLit(vertex_map[v2_ind][w1_ind].first));
      solver.addClause(Glucose::mkLit(edge_map[e_ind][f_ind].first, true),
                       Glucose::mkLit(vertex_map[v1_ind][w2_ind].first),
                       Glucose::mkLit(vertex_map[v2_ind][w2_ind].first));
    }
  }

  //couple tree_arcs with vertex_map
  for (size_t i = 0; i < simple_arcs.size(); i++) {
    auto v1_ind = get(vertex_id_G, simple_arcs[i].first);
    auto v2_ind = get(vertex_id_G, simple_arcs[i].second);
    for (size_t w = 0; w < num_vertices(H); w++) {
      solver.addClause(Glucose::mkLit(tree_arcs[i][w].first, true), Glucose::mkLit(vertex_map[v1_ind][w].first));
      solver.addClause(Glucose::mkLit(tree_arcs[i][w].first, true), Glucose::mkLit(vertex_map[v2_ind][w].first));
    }
  }

  //couple tree_roots with vertex_map
  for (size_t i = 0; i < num_vertices(G); i++) {
    for (size_t j = 0; j < num_vertices(H); j++) {
      solver.addClause(Glucose::mkLit(tree_roots[i][j].first, true), Glucose::mkLit(vertex_map[i][j].first));
    }
  }

  //tree roots have no incoming tree arcs
  auto vs = vertices(G);
  for (auto v_it = vs.first; v_it != vs.second; v_it++) {
    int v_ind = get(vertex_id_G, *v_it);
    for (size_t i = 0; i < simple_arcs.size(); i++) {
      if (simple_arcs[i].second == *v_it) {
        for (size_t w = 0; w < num_vertices(H); w++) {
          solver.addClause(Glucose::mkLit(tree_roots[v_ind][w].first, true),
                           Glucose::mkLit(tree_arcs[i][w].first, true));
        }
      }
    }
  }

  //non-tree roots have at least one incoming arc
  for (auto v_it = vs.first; v_it != vs.second; v_it++) {
    int v_ind = get(vertex_id_G, *v_it);
    for (size_t w = 0; w < num_vertices(H); w++) {
      Glucose::vec<Glucose::Lit> clause;
      clause.push(Glucose::mkLit(vertex_map[v_ind][w].first, true));
      clause.push(Glucose::mkLit(tree_roots[v_ind][w].first));
      for (size_t i = 0; i < simple_arcs.size(); i++) {
        if (simple_arcs[i].second == *v_it) {
          clause.push(Glucose::mkLit(tree_arcs[i][w].first));
        }
      }
      solver.addClause(clause);
    }
  }

  // define closure
  for (size_t i = 0; i < simple_arcs.size(); i++) {
    auto v1_ind = get(vertex_id_G, simple_arcs[i].first);
    auto v2_ind = get(vertex_id_G, simple_arcs[i].second);
    for (size_t w = 0; w < num_vertices(H); w++) {
      solver.addClause(Glucose::mkLit(tree_arcs[i][w].first, true), Glucose::mkLit(closure[v1_ind][v2_ind].first));
    }
  }
  // transitive closure
  for (size_t v1 = 0; v1 < num_vertices(G); v1++) {
    for (size_t v2 = 0; v2 < num_vertices(G); v2++) {
      for (size_t v3 = 0; v3 < num_vertices(G); v3++) {
        solver.addClause(Glucose::mkLit(closure[v1][v2].first, true), Glucose::mkLit(closure[v2][v3].first, true),
                         Glucose::mkLit(closure[v1][v3].first));
      }
    }
  }
  //forbid loops
  for (size_t v = 0; v < num_vertices(G); v++) {
    solver.addClause(Glucose::mkLit(closure[v][v].first, true));
  }

  /*solver.setFrozen(vertex_map[0][0].first, true);
  solver.setFrozen(vertex_map[1][1].first, true);
  solver.setFrozen(vertex_map[2][2].first, true);
  solver.setFrozen(vertex_map[3][0].first, true);
  solver.setFrozen(vertex_map[4][2].first, true);
  solver.setFrozen(vertex_map[5][3].first, true);
  solver.setFrozen(vertex_map[6][4].first, true);*/
  /*solver.addClause(Glucose::mkLit(vertex_map[0][0].first));
  solver.addClause(Glucose::mkLit(vertex_map[1][1].first));
  solver.addClause(Glucose::mkLit(vertex_map[2][2].first));
  solver.addClause(Glucose::mkLit(vertex_map[3][0].first));
  solver.addClause(Glucose::mkLit(vertex_map[4][2].first));
  solver.addClause(Glucose::mkLit(vertex_map[5][3].first));
  solver.addClause(Glucose::mkLit(vertex_map[6][4].first));*/

  //solver.toDimacs("before");
  modellingTime = (double)(std::clock() - c_start) / CLOCKS_PER_SEC;
  c_start = std::clock();
  bool res = solver.solve();
  solvingTime = (double)(std::clock() - c_start) / CLOCKS_PER_SEC;


  //solver.toDimacs("after");
  //std::cout << res << std::endl;
  //boost::print_edges2(G, vertex_id_G, edge_id_G);
  //boost::print_edges2(H, vertex_id_H, edge_id_H);
  /*if (!res) {
    boost::print_graph(G);
  } else {
    print(solver, vertex_map);
    print(solver, edge_map);
    print(solver, tree_arcs);
    print(solver, tree_roots);
    print(solver, closure);
  }*/
  //exit(0);
  return res;
}


#endif //PPM_TESTING_K5MINORSOLVERSAT_H
