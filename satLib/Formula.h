//
// Created by benedikt on 11/2/18.
//

#ifndef PPM_TESTING_FORMULATREE_H
#define PPM_TESTING_FORMULATREE_H

#include <utility>
#include <vector>
#include <string>
#include <memory>
#include <iostream>
#include "CNF.h"
#include <exception>
#include <queue>
#include <deque>
#include <unordered_map>

namespace satLib {

  enum TruthValue {
      True, False, Unknown
  };

  class Variable;

  class Formula;

  namespace internal {
    class Variable;

    class Formula {
        friend std::ostream& operator<<(std::ostream& os, const Formula& obj) {
          obj.print(os);
          return os;
        }

      public:
        typedef std::deque<Clause<const Variable*>> Clauses;
        typedef std::vector<Literal<const Variable*>> SimpleConjunction;
        typedef std::unordered_map<const Variable*, bool> FixedVariables;

        virtual void addToCNFAlgebraically(Clauses& clauses, bool positive) const = 0;

        virtual bool isLiteral() const { return false; }

        virtual bool isTrue() const { return false; }

        virtual bool isFalse() const { return false; }

        virtual bool isClause() const { return isLiteral(); }

        virtual bool isSimpleConjunction() const { return isLiteral(); }

        virtual Literal<const Variable*> toLiteral(bool positive) const {
          throw std::runtime_error("Is no literal");
        }

        virtual Clause<const Variable*> toClause(bool positive) const {
          return Clause<const Variable*>({toLiteral(positive)});
        }

        virtual SimpleConjunction toSimpleConjunction(bool positive) const {
          return {toLiteral(positive)};
        }

        virtual void print(std::ostream& os) const = 0;

        virtual bool bracketNeg() const { return false; }

        virtual bool bracketOr() const { return false; }

        virtual bool bracketAnd() const { return false; }

        virtual bool bracketImpl() const { return false; }

        virtual bool bracketEquiv() const { return false; }

        virtual bool isOr() const { return false; }

        virtual bool isAnd() const { return false; }

        virtual void printOneConstraintPerRow(std::ostream& os) {
          os << *this;
        }

        virtual TruthValue truthForFixedVariables(FixedVariables& fixedVariables) = 0;
    };

    class True : public Formula {
      public:
        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override {}

        void print(std::ostream& os) const override {
          os << "T";
        }

        bool isTrue() const override {
          return true;
        }

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          return TruthValue::True;
        }
    };

    class False : public Formula {
      public:
        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override {
          clauses.push_front(Clause<const Variable*>({}));
        }

        void print(std::ostream& os) const override {
          os << "F";
        }

        bool isFalse() const override {
          return true;
        }

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          return TruthValue::False;
        }
    };

    class Variable : public Formula {
        friend satLib::Variable;
      public:
        const std::string& getName() const {
          return name;
        }

        bool isLiteral() const override { return true; }

        Literal<const Variable*> toLiteral(bool positive) const override {
          return Literal<const Variable*>(this, positive);
        }

        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override;

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          if (fixedVariables.count(this) > 0) {
            return fixedVariables[this] ? TruthValue::True : TruthValue::False;
          }
          return TruthValue::Unknown;
        }

      private:
        void print(std::ostream& os) const override;

        explicit Variable(std::string name) : name(std::move(name)) {}

        std::string name;
    };

    void Variable::addToCNFAlgebraically(Clauses& clauses, bool positive) const {
      clauses.push_front(toClause(positive));
    }

    void Variable::print(std::ostream& os) const {
      os << name;
    }

    class Operator : public Formula {
      protected:
        virtual bool bracket(const Formula& f) const = 0;

        virtual void printOperator(std::ostream& os) const = 0;
    };

    class Unary : public Operator {
        friend satLib::Formula;
      protected:
        explicit Unary(std::shared_ptr<Formula> formula) : formula(std::move(formula)) {}

        std::shared_ptr<Formula> formula;
      private:
        void print(std::ostream& os) const override;
    };

    void Unary::print(std::ostream& os) const {
      printOperator(os);
      bool br = bracket(*formula);
      if (br) {
        os << "(";
      }
      os << *formula;
      if (br) {
        os << ")";
      }
    }

    class Neg : public Unary {
      public:
        explicit Neg(const std::shared_ptr<Formula>& formula) : Unary(formula), _isLiteral(formula->isLiteral()),
                                                                _isClause(formula->isSimpleConjunction()),
                                                                _isSimpleConjunction(formula->isClause()) {}

        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override;

        bool isLiteral() const override { return _isLiteral; }


        Literal<const Variable*> toLiteral(bool positive) const override;

        Clause<const Variable*> toClause(bool positive) const override;

        SimpleConjunction toSimpleConjunction(bool positive) const override;

        bool isClause() const override { return _isClause; }

        bool isSimpleConjunction() const override { return _isSimpleConjunction; }

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          switch (formula->truthForFixedVariables(fixedVariables)) {
          case TruthValue::True: {
            return TruthValue::False;
            break;
          }
          case TruthValue::False: {
            return TruthValue::True;
            break;
          }
          default: {
            return TruthValue::Unknown;
            break;
          }
          }
        }

      protected:
        bool bracket(const Formula& f) const override {
          return f.bracketNeg();
        }

        void printOperator(std::ostream& os) const override {
          os << "-";
        }

      private:
        bool _isLiteral;
        bool _isClause;
        bool _isSimpleConjunction;
    };

    void Neg::addToCNFAlgebraically(Clauses& clauses, bool positive) const {
      return formula->addToCNFAlgebraically(clauses, !positive);
    }

    Literal<const Variable*> Neg::toLiteral(bool positive) const {
      return formula->toLiteral(!positive);
    }

    Clause<const Variable*> Neg::toClause(bool positive) const {
      return formula->toClause(!positive);
    }

    Formula::SimpleConjunction Neg::toSimpleConjunction(bool positive) const {
      return formula->toSimpleConjunction(!positive);
    }


    class Binary : public Operator {
        bool bracketNeg() const override { return true; }

        friend satLib::Formula;
      public:
        Binary(std::shared_ptr<Formula> f1, std::shared_ptr<Formula> f2) : f1(std::move(f1)), f2(std::move(f2)) {}

      protected:
        std::shared_ptr<Formula> f1;
        std::shared_ptr<Formula> f2;
      private:
        void print(std::ostream& os) const override;
    };

    void Binary::print(std::ostream& os) const {
      bool br1 = bracket(*f1);
      if (br1) { os << "("; }
      os << *f1;
      if (br1) { os << ")"; }

      printOperator(os);

      bool br2 = bracket(*f2);
      if (br2) { os << "("; }
      os << *f2;
      if (br2) { os << ")"; }
    }

    Formula::SimpleConjunction mergeVectors(Formula::SimpleConjunction v1, Formula::SimpleConjunction v2) {
      if (v1.size() >= v2.size()) {
        v1.insert(v1.end(), v2.begin(), v2.end());
        return v1;
      } else {
        v2.insert(v2.end(), v1.begin(), v1.end());
        return v2;
      }
    }

    Clause<const Variable*> mergeClauses(Clause<const Variable*> c1, Clause<const Variable*> c2) {
      if (c1.getLiterals().size() >= c2.getLiterals().size()) {
        c1.getLiterals().insert(c1.getLiterals().end(), c2.getLiterals().begin(), c2.getLiterals().end());
        return c1;
      } else {
        c2.getLiterals().insert(c2.getLiterals().end(), c1.getLiterals().begin(), c1.getLiterals().end());
        return c2;
      }
    }

    class Or : public Binary {

        bool bracketAnd() const override { return true; }

      public:
        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override;

      public:
        Or(std::shared_ptr<Formula> f1, std::shared_ptr<Formula> f2) : Binary(std::move(f1), std::move(f2)), _isClause(
            this->f1->isClause() && this->f2->isClause()) {}

        bool isClause() const override {
          return _isClause;
        }

        Clause<const Variable*> toClause(bool positive) const override {
          assert(positive);
          return mergeClauses(f1->toClause(positive), f2->toClause(positive));
        }

        SimpleConjunction toSimpleConjunction(bool positive) const override {
          assert(!positive);
          return mergeVectors(f1->toSimpleConjunction(positive), f2->toSimpleConjunction(positive));
        }

        bool isOr() const override { return true; }

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          TruthValue v1 = f1->truthForFixedVariables(fixedVariables);
          if (v1 == TruthValue::True) {
            return TruthValue::True;
          }
          TruthValue v2 = f2->truthForFixedVariables(fixedVariables);
          if (v2 == TruthValue::True) {
            return TruthValue::True;
          }
          if (v1 == TruthValue::Unknown || v2 == TruthValue::Unknown) {
            return TruthValue::Unknown;
          } else {
            return TruthValue::False;
          }
        }

      protected:
        bool bracket(const Formula& f) const override {
          return f.bracketOr();
        }

        void printOperator(std::ostream& os) const override {
          os << "|";
        }

      private:
        bool _isClause;
    };

    void simpleConjunctionToCNF(Formula::Clauses& clauses, Formula::SimpleConjunction simpleConjunction) {
      for (auto& lit: simpleConjunction) {
        clauses.push_front(Clause<const Variable*>({lit}));
      }
    }

    void outerOr(Formula::Clauses& clauses, const Formula::Clauses& cl1, const Formula::Clauses& cl2) {
      for (auto& c1: cl1) {
        for (auto& c2: cl2) {
          clauses.push_front(mergeClauses(c1, c2));
        }
      }
    }

    void addToCNFOrOrQueue(Formula::Clauses& clauses, std::queue<const Or*>& ors, const Formula* f) {
      if (f->isOr()) {
        ors.push(dynamic_cast<const Or*>(f));
      } else {
        f->addToCNFAlgebraically(clauses, false);
      }
    }

    void Or::addToCNFAlgebraically(Clauses& clauses, bool positive) const {
      if (positive && isClause()) {
        clauses.push_front(toClause(true));
        return;
      }
      if (!positive && isClause()) {
        simpleConjunctionToCNF(clauses, toSimpleConjunction(false));
        return;
      }
      if (positive) {
        Clauses cl1;
        f1->addToCNFAlgebraically(cl1, true);
        Clauses cl2;
        f2->addToCNFAlgebraically(cl2, true);
        outerOr(clauses, cl1, cl2);
      } else {
        std::queue<const Or*> ors;
        ors.push(this);
        while (!ors.empty()) {
          auto o = ors.front();
          ors.pop();
          addToCNFOrOrQueue(clauses, ors, o->f1.get());
          addToCNFOrOrQueue(clauses, ors, o->f2.get());
        }
      }
    }

    class And : public Binary {

        bool bracketOr() const override { return true; }

      public:
        void printOneConstraintPerRow(std::ostream& os) override {
          f1->printOneConstraintPerRow(os);
          os << std::endl;
          f2->printOneConstraintPerRow(os);
        }

        bool isSimpleConjunction() const override {
          return _isSimpleConjunction;
        }

        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override;

        SimpleConjunction toSimpleConjunction(bool positive) const override {
          assert(positive);
          return mergeVectors(f1->toSimpleConjunction(true), f2->toSimpleConjunction(true));
        }

        Clause<const Variable*> toClause(bool positive) const override {
          assert(!positive);
          return mergeClauses(f1->toClause(false), f2->toClause(false));
        }

        And(std::shared_ptr<Formula> f1, std::shared_ptr<Formula> f2) : Binary(std::move(f1), std::move(f2)),
                                                                        _isSimpleConjunction(
                                                                            this->f1->isSimpleConjunction() &&
                                                                            this->f2->isSimpleConjunction()) {}

        bool isAnd() const override { return true; }

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          TruthValue v1 = f1->truthForFixedVariables(fixedVariables);
          if (v1 == TruthValue::False) {
            return TruthValue::False;
          }
          TruthValue v2 = f2->truthForFixedVariables(fixedVariables);
          if (v2 == TruthValue::False) {
            return TruthValue::False;
          }
          if (v1 == TruthValue::Unknown || v2 == TruthValue::Unknown) {
            return TruthValue::Unknown;
          } else {
            return TruthValue::True;
          }
        }

      protected:

        bool bracket(const Formula& f) const override {
          return f.bracketAnd();
        }

        void printOperator(std::ostream& os) const override {
          os << "&";
        }

      private:
        bool _isSimpleConjunction;
    };

    void addToCNFOrAndQueue(Formula::Clauses& clauses, std::queue<const And*>& ands, const Formula* f) {
      if (f->isAnd()) {
        ands.push(dynamic_cast<const And*>(f));
      } else {
        f->addToCNFAlgebraically(clauses, true);
      }
    }

    void And::addToCNFAlgebraically(Clauses& clauses, bool positive) const {
      if (!positive && isSimpleConjunction()) {
        clauses.push_front(toClause(false));
        return;
      }
      if (positive && isSimpleConjunction()) {
        simpleConjunctionToCNF(clauses, toSimpleConjunction(true));
        return;
      }
      if (positive) {
        std::queue<const And*> ands;
        ands.push(this);
        while (!ands.empty()) {
          auto a = ands.front();
          ands.pop();
          addToCNFOrAndQueue(clauses, ands, a->f1.get());
          addToCNFOrAndQueue(clauses, ands, a->f2.get());
        }
      } else {
        Clauses cl1;
        f1->addToCNFAlgebraically(cl1, false);
        Clauses cl2;
        f2->addToCNFAlgebraically(cl2, false);
        outerOr(clauses, cl1, cl2);
      }
    }

    class Implies : public Binary {

        bool bracketImpl() const override { return true; }

        bool bracketEquiv() const override { return true; }

        bool bracketAnd() const override { return true; }

        bool bracketOr() const override { return true; }

      public:
        bool isClause() const override {
          return _isClause;
        }

        Clause<const Variable*> toClause(bool positive) const override {
          assert(positive);
          return mergeClauses(f1->toClause(false), f2->toClause(true));
        }

        SimpleConjunction toSimpleConjunction(bool positive) const override {
          assert(!positive);
          return mergeVectors(f1->toSimpleConjunction(true), f2->toSimpleConjunction(false));
        }

        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override;

        Implies(std::shared_ptr<Formula> f1, std::shared_ptr<Formula> f2) : Binary(std::move(f1), std::move(f2)),
                                                                            _isClause(this->f1->isSimpleConjunction() &&
                                                                                      this->f2->isClause()) {}

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          TruthValue v1 = f1->truthForFixedVariables(fixedVariables);
          if (v1 == TruthValue::False) {
            return TruthValue::True;
          }
          TruthValue v2 = f2->truthForFixedVariables(fixedVariables);
          if (v2 == TruthValue::True) {
            return TruthValue::True;
          }
          if (v1 == TruthValue::Unknown || v2 == TruthValue::Unknown) {
            return TruthValue::Unknown;
          } else {
            return TruthValue::False;
          }
        }

      protected:

        bool bracket(const Formula& f) const override {
          return f.bracketImpl();
        }

        void printOperator(std::ostream& os) const override {
          os << "->";
        }

      private:
        bool _isClause;
    };

    void Implies::addToCNFAlgebraically(Clauses& clauses, bool positive) const {
      if (positive && isClause()) {
        clauses.push_front(toClause(true));
        return;
      }
      if (!positive && isClause()) {
        simpleConjunctionToCNF(clauses, toSimpleConjunction(false));
        return;
      }
      if (positive) {
        Clauses cl1;
        f1->addToCNFAlgebraically(cl1, false);
        Clauses cl2;
        f2->addToCNFAlgebraically(cl2, true);
        outerOr(clauses, cl1, cl2);
      } else {
        f1->addToCNFAlgebraically(clauses, true);
        f2->addToCNFAlgebraically(clauses, false);
      }
    }

    class Equivalent : public Binary {
        using Binary::Binary;

        bool bracketImpl() const override { return true; }

        bool bracketEquiv() const override { return true; }

        bool bracketAnd() const override { return true; }

        bool bracketOr() const override { return true; }

      public:
        void addToCNFAlgebraically(Clauses& clauses, bool positive) const override;

        TruthValue truthForFixedVariables(FixedVariables& fixedVariables) override {
          TruthValue v1 = f1->truthForFixedVariables(fixedVariables);
          TruthValue v2 = f2->truthForFixedVariables(fixedVariables);
          if (v1 == TruthValue::Unknown || v2 == TruthValue::Unknown) {
            return TruthValue::Unknown;
          } else if (v1 == v2) {
            return TruthValue::True;
          } else {
            return TruthValue::False;
          }
        }

      protected:
        bool bracket(const Formula& f) const override {
          return f.bracketEquiv();
        }

        void printOperator(std::ostream& os) const override {
          os << "<->";
        }
    };
  }

  typedef std::unordered_map<const Variable*, bool> FixedVariables;

  class Formula {
      friend internal::Equivalent;
    public:
      static Formula getTrue() {
        return Formula(std::shared_ptr<internal::Formula>(new internal::True()));
      }

      static Formula getFalse() {
        return Formula(std::shared_ptr<internal::Formula>(new internal::False()));
      }

      Formula operator-() const;

      Formula operator|(Formula f2) const;

      Formula& operator|=(Formula f2);

      Formula operator&(Formula f2) const;

      Formula& operator&=(Formula f2);


      Formula operator>>(Formula f2) const;

      Formula equiv(Formula f2) const;

      friend std::ostream& operator<<(std::ostream& os, const Formula& obj) {
        os << *obj.formula;
        return os;
      }

      void printOneConstraintPerRow(std::ostream& os) {
        formula->printOneConstraintPerRow(os);
      }

      CNF<const internal::Variable*> convertToCNFAlgebraically() const {
        internal::Formula::Clauses clauses;
        formula->addToCNFAlgebraically(clauses, true);
        return CNF<const internal::Variable*>(clauses);
      }

      TruthValue truthForFixedVariables(FixedVariables& fixedVariables) const;

    protected:
      explicit Formula(std::shared_ptr<internal::Formula> formula) : formula(std::move(formula)) {}

      std::shared_ptr<internal::Formula> formula;
  };

  class Variable : public Formula {
    public:
      explicit Variable(const std::string& name) : Formula(
          std::shared_ptr<internal::Formula>(new internal::Variable(name))) {}

      const std::string& getName() const {
        return std::static_pointer_cast<internal::Variable>(formula)->getName();
      }
  };

  TruthValue Formula::truthForFixedVariables(FixedVariables& fixedVariables) const {
    internal::Formula::FixedVariables fixedVariables2;
    for (auto p: fixedVariables) {
      fixedVariables2.insert(
          std::make_pair(std::static_pointer_cast<internal::Variable>(p.first->formula).get(), p.second));
    }
    return formula->truthForFixedVariables(fixedVariables2);
  }

  Formula Formula::operator-() const {
    return Formula(std::shared_ptr<internal::Formula>(new internal::Neg(formula)));
  }

  Formula Formula::operator|(Formula f2) const {
    if (this->formula->isFalse()) {
      return f2;
    } else if (f2.formula->isFalse()) {
      return *this;
    }
    return Formula(std::shared_ptr<internal::Formula>(new internal::Or(formula, f2.formula)));
  }

  Formula& Formula::operator|=(Formula f2) {
    if (formula->isFalse()) {
      formula = f2.formula;
    } else if (!f2.formula->isFalse()) {
      formula = std::shared_ptr<internal::Formula>(new internal::Or(formula, f2.formula));
    }
    return *this;
  }

  Formula Formula::operator&(Formula f2) const {
    if (this->formula->isTrue()) {
      return f2;
    } else if (f2.formula->isTrue()) {
      return *this;
    }
    return Formula(std::shared_ptr<internal::Formula>(new internal::And(formula, f2.formula)));
  }

  Formula& Formula::operator&=(Formula f2) {
    if (formula->isTrue()) {
      formula = f2.formula;
    } else if (!f2.formula->isTrue()) {
      formula = std::shared_ptr<internal::Formula>(new internal::And(formula, f2.formula));
    }
    return *this;
  }

  Formula Formula::operator>>(Formula f2) const {
    return Formula(std::shared_ptr<internal::Formula>(new internal::Implies(formula, f2.formula)));
  }

  Formula Formula::equiv(satLib::Formula f2) const {
    return Formula(std::shared_ptr<internal::Formula>(new internal::Equivalent(formula, f2.formula)));
  }

  void internal::Equivalent::addToCNFAlgebraically(internal::Formula::Clauses& clauses, bool positive) const {
    satLib::Formula tmp1 = satLib::Formula(f1);
    satLib::Formula tmp2 = satLib::Formula(f2);
    ((tmp1 >> tmp2) & (tmp2 >> tmp1)).formula->addToCNFAlgebraically(clauses, positive);
  }
}


#endif //PPM_TESTING_FORMULATREE_H
