//
// Created by benedikt on 11/2/18.
//

#ifndef PPM_TESTING_CNF_H
#define PPM_TESTING_CNF_H

#include <vector>
#include <deque>
#include <iostream>
#include <fstream>

namespace satLib {
  template<class Variable>
  class Literal {
    public:
      explicit Literal(Variable v, bool positive = true);

      bool isPositive() const {
        return positive;
      }

      void setPositive(bool positive) {
        Literal::positive = positive;
      }

      Variable getV() const {
        return v;
      }

    private:
      Variable v;
      bool positive;
  };

  template<class Variable>
  Literal<Variable>::Literal(Variable v, bool positive):v(v), positive(positive) {}

  template<class Variable>
  class Clause {
    public:
      explicit Clause(const std::vector<Literal<Variable>>& literals);

      const std::vector<Literal<Variable>>& getLiterals() const {
        return literals;
      }

      std::vector<Literal<Variable>>& getLiterals() {
        return literals;
      }

    private:
      std::vector<Literal<Variable>> literals;
  };

  template<class Variable>
  Clause<Variable>::Clause(const std::vector<Literal<Variable>>& literals):literals(literals) {}

  template<class Variable>
  class CNF {
    public:
      explicit CNF(const std::deque<Clause<Variable>>& clauses);

      const std::deque<Clause<Variable>>& getClauses() const {
        return clauses;
      }

      std::deque<Clause<Variable>>& getClauses() {
        return clauses;
      }

      void print(const std::string& filename) const;

    private:
      std::deque<Clause<Variable>> clauses;
  };

  template<class Variable>
  CNF<Variable>::CNF(const std::deque<Clause<Variable>>& clauses):clauses(clauses) {}

  template<class Variable>
  void CNF<Variable>::print(const std::string& filename) const {
    std::unordered_map<Variable, size_t> vars;
    size_t var_id = 0;
    std::vector<Variable> indexedVars;

    for(const Clause<Variable>& clause: clauses) {
      for (const Literal<Variable>& lit: clause.getLiterals()) {
        Variable v = lit.getV();
        if (vars.count(v) == 0) {
          vars.insert(std::make_pair(v, ++var_id));
        }
      }
    }

    std::ofstream outFile;
    outFile.open(filename, std::ios::out | std::ios::trunc);
    outFile << "p cnf " << var_id << " " << clauses.size();

    for(const Clause<Variable>& clause: clauses) {
      outFile << std::endl;
      for (const Literal<Variable>& lit: clause.getLiterals()) {
        if (!lit.isPositive()) {
          outFile << "-";
        }
        outFile << vars[lit.getV()] << " ";
      }
      outFile << "0";
    }

    outFile.close();
  }
}


#endif //PPM_TESTING_CNF_H
