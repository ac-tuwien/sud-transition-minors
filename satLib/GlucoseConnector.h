//
// Created by benedikt on 11/2/18.
//

#ifndef PPM_TESTING_GLUCOSECONNECTOR_H
#define PPM_TESTING_GLUCOSECONNECTOR_H


#include <unordered_map>
#include "CNF.h"
#include "simp/SimpSolver.h" //shield names from glucose in this namespace
#include <iostream>

namespace satLib {
  namespace Glucose {
    template<class Var>
    bool solve(const CNF<Var>& cnf) {
      std::unordered_map<Var, ::Glucose::Var> vars;
      std::vector<Var> indexedVars;
      typedef ::Glucose::SimpSolver SATSolver;
      SATSolver solver;
      solver.parsing = 1;
      solver.use_simplification = true;
      solver.verbosity = -100;
      solver.verbEveryConflicts = 1000000;
      solver.showModel = false;
      solver.certifiedUNSAT = false;
      solver.vbyte = false;

      for(const Clause<Var>& clause: cnf.getClauses()) {
        ::Glucose::vec<::Glucose::Lit> cl;
        for (const Literal<Var>& lit: clause.getLiterals()) {
          Var v = lit.getV();
          if (vars.count(v) == 0) {
            vars.insert(std::make_pair(v, solver.newVar()));
            indexedVars.push_back(v);
          }
          cl.push(::Glucose::mkLit(vars[v], !lit.isPositive()));
        }
        solver.addClause(cl);
      }
      solver.parsing = 0;
      solver.eliminate(true);

      if (!solver.okay()) {
        //simplification already solved it!
        return false;
      }

      ::Glucose::vec<::Glucose::Lit> dummy;
      ::Glucose::lbool ret = solver.solveLimited(dummy);

      return ret == ::Glucose::lbool((uint8_t)0);
    }
  }
}




#endif //PPM_TESTING_GLUCOSECONNECTOR_H
