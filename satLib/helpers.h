//
// Created by benedikt on 11/2/18.
//

#ifndef PPM_TESTING_HELPERS_H
#define PPM_TESTING_HELPERS_H

#include "Formula.h"
#include <vector>

namespace satLib {
  typedef std::vector<std::vector<Variable>> Relation;

  Relation
  createRelation(size_t domainSize, size_t imageSize, const std::string& relationName, bool isFunction = true) {
    Relation res(domainSize);
    for (size_t i = 0; i < domainSize; i++) {
      res[i].reserve(imageSize);
      for (size_t j = 0; j < imageSize; j++) {
        std::string name;
        if (isFunction) {
          name = relationName + "(" + std::to_string(i) + ") = " + std::to_string(j);
        } else {
          name = std::to_string(i) + relationName + std::to_string(j);
        }
        res[i].push_back(satLib::Variable(name));
      }
    }
    return res;
  }

  Formula partial(Relation& relation) {
    auto res = Formula::getTrue();
    for (auto& images: relation) {
      for (size_t j = 0; j < images.size(); j++) {
        for (size_t k = j + 1; k < images.size(); k++) {
          res &= -images[j] | -images[k];
        }
      }
    }
    return res;
  }

  Relation
  createPartialFunction(size_t domainSize, size_t imageSize, Formula& toAppend, const std::string& relationName) {
    auto function = createRelation(domainSize, imageSize, relationName, true);
    toAppend &= partial(function);
    return function;
  }

  Formula injective(Relation& relation) {
    auto res = Formula::getTrue();
    for (size_t i = 0; i < relation.size(); i++) {
      for (size_t j = i + 1; j < relation.size(); j++) {
        for (size_t k = 0; k < relation[i].size(); k++) {
          res &= -relation[i][k] | -relation[j][k];
        }
      }
    }
    return res;
  }

  Formula surjective(Relation& relation) {
    if (relation.empty()) {
      return Formula::getTrue();
    }
    auto res = Formula::getTrue();
    for (size_t j = 0; j < relation[0].size(); j++) {
      auto temp = Formula::getFalse();
      for (auto& images: relation) {
        temp |= images[j];
      }
      res &= temp;
    }
    return res;
  }

  Formula function(Relation& relation) {
    auto res = Formula::getTrue();
    for (auto& images: relation) {
      auto temp = Formula::getFalse();
      for (auto& image: images) {
        temp |= image;
      }
      res &= temp;
    }
    return res;
  }

  Relation
  createFunction(size_t domainSize, size_t imageSize, Formula& toAppend, const std::string& relationName) {
    auto f = createPartialFunction(domainSize, imageSize, toAppend, relationName);
    toAppend &= function(f);
    return f;
  }
}

#endif //PPM_TESTING_HELPERS_H
