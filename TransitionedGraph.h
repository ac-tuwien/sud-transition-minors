//
// Created by benedikt on 10/24/18.
//

#ifndef PPM_TESTING_TRANSITIONEDGRAPH_H
#define PPM_TESTING_TRANSITIONEDGRAPH_H

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/named_graph.hpp>
#include <vector>

template<class Vertex, class Edge>
class Transition {
  public:
    Transition(Vertex v, Edge e1, Edge e2) : v(v), e1(e1), e2(e2) {}

    Vertex getV() const {
      return v;
    }

    void setV(Vertex v) {
      Transition::v = v;
    }

    Edge getE1() const {
      return e1;
    }

    void setE1(Edge e1) {
      Transition::e1 = e1;
    }

    Edge getE2() const {
      return e2;
    }

    void setE2(Edge e2) {
      Transition::e2 = e2;
    }

  private:
    Vertex v;
    Edge e1;
    Edge e2;
};

template<class Graph>
class TransitionedGraph {
  public:
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
    typedef typename boost::graph_traits<Graph>::edge_descriptor Edge;
    const Graph& getGraph() const {
      return graph;
    }

    Graph& getGraph() {
      return graph;
    }

    void setGraph(Graph graph) {
      TransitionedGraph::graph = graph;
    }

    const std::vector<Transition<Vertex, Edge>>& getTransitions() const {
      return transitions;
    }

    void setTransitions(const std::vector<Transition<Vertex, Edge>>& transitions) {
      transition_inds = std::vector<std::vector<size_t>>(num_vertices(graph));
      this->transitions = transitions;
      for (size_t i = 0; i < this->transitions.size(); i++) {
        transition_inds[this->transitions[i].getV()].push_back(i);
      }
    }

    const std::vector<size_t>& getTransitionIndices(Vertex v) const {
      return transition_inds[v];
    }

    void printTo(std::ostream& os, std::string gName, std::string tName) const;

  private:
    Graph graph;
    std::vector<Transition<Vertex, Edge>> transitions;
    std::vector<std::vector<size_t>> transition_inds;
};

template<class Graph>
void TransitionedGraph<Graph>::printTo(std::ostream& os, std::string gName, std::string tName) const {
  os << "#" << gName << std::endl;
  auto edge_id = boost::get(boost::edge_index, graph);
  auto vertex_id = boost::get(boost::vertex_index, graph);
  auto es = edges(graph);
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto e = get(edge_id, *e_it);
    auto v1 = get(vertex_id, source(*e_it, graph));
    auto v2 = get(vertex_id, target(*e_it, graph));
    os << e << ": " << v1 << " " << v2 << std::endl;
  }
  os << "#" << tName << std::endl;
  bool first = true;
  for (auto& transition: transitions) {
    auto v = get(vertex_id, transition.getV());
    auto e1 = get(edge_id, transition.getE1());
    auto e2 = get(edge_id, transition.getE2());
    if (first) {
      first = false;
    } else {
      os << std::endl;
    }
    os << v << " " << e1 << " " << e2;
  }
}


#endif //PPM_TESTING_TRANSITIONEDGRAPH_H
