//
// Created by benedikt on 11/14/18.
//

#ifndef PPM_TESTING_AUTOMORPHISMGROUP_H
#define PPM_TESTING_AUTOMORPHISMGROUP_H

#define _unused(x) ((void)(x))

#include <vector>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/named_graph.hpp>
#include <nauty.h>
#include <assert.h>
#include <deque>
#include "TransitionedGraph.h"
#include <schreier.h>
#include <nausparse.h>

template<class Graph>
class Automorphism {
  public:
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;

    Automorphism(std::vector<Vertex> image) : image(image) {}

    void composeWithOuter(const Automorphism<Graph>& aut) {
      for (int i = 0; i < image.size(); i++) {
        image[i] = aut.image[image[i]];
      }
    }

    Automorphism<Graph> inverse() const {
      std::vector<Vertex> iimage(image.size());
      for (int i = 0; i < image.size(); i++) {
        iimage[image[i]] = i;
      }
      return Automorphism<Graph>(iimage);
    }

  public:
    std::vector<Vertex> image;
};

template<class Graph>
class AutomorphismGroup {
  public:
    typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;

    AutomorphismGroup(std::vector<Vertex> stabilizerVertices, std::vector<std::vector<Automorphism<Graph>>> generators,
                      std::vector<std::unordered_set<Vertex>> stabilizerOrbits,
                      std::vector<std::unordered_set<Vertex>> orbits) : stabilizerVertices(stabilizerVertices),
                                                                        generators(generators),
                                                                        stabilizerOrbits(stabilizerOrbits),
                                                                        orbits(orbits) {}

    const std::vector<Vertex>& getStabilizerVertices() const {
      return stabilizerVertices;
    }

    const std::vector<std::vector<Automorphism<Graph>>>& getGenerators() const {
      return generators;
    }

    const std::vector<std::unordered_set<Vertex>>& getStabilizerOrbits() const {
      return stabilizerOrbits;
    }

    const std::vector<std::unordered_set<Vertex>>& getOrbits() const {
      return orbits;
    }

  private:
    std::vector<Vertex> stabilizerVertices;
    std::vector<std::vector<Automorphism<Graph>>> generators; //grouped by stabilizers
    std::vector<std::unordered_set<Vertex>> stabilizerOrbits; //the orbit of vertex w_i in stabilizer of w_0,...,w_{i-1}
    std::vector<std::unordered_set<Vertex>> orbits;
};

namespace internal {
  template<class Graph>
  class TmpGenerators {
    public:
      typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
      static std::deque<std::vector<Automorphism<Graph>>> generators;
      static std::deque<std::unordered_set<Vertex>> stabilizerOrbits;
      static std::deque<Vertex> stabilizerVertices;
  };

  template<class Graph> typename std::deque<std::vector<Automorphism<Graph>>> TmpGenerators<Graph>::generators;

  template<class Graph> typename std::deque<std::unordered_set<typename TmpGenerators<Graph>::Vertex>> TmpGenerators<Graph>::stabilizerOrbits;

  template<class Graph> typename std::deque<typename TmpGenerators<Graph>::Vertex> TmpGenerators<Graph>::stabilizerVertices;

  template<class Graph>
  void userautomproc(int count, int* perm, int* orbits, int numorbits, int stabvertex, int n) {
    if (TmpGenerators<Graph>::stabilizerVertices.empty() ||
        TmpGenerators<Graph>::stabilizerVertices.front() != (size_t)stabvertex) {
      //new stabvertex
      TmpGenerators<Graph>::stabilizerVertices.push_front(stabvertex);
      TmpGenerators<Graph>::generators.push_front(std::vector<Automorphism<Graph>>());
      TmpGenerators<Graph>::stabilizerOrbits.push_front(std::unordered_set<typename TmpGenerators<Graph>::Vertex>());
    }
    //add generator
    TmpGenerators<Graph>::generators.front().push_back(
        Automorphism<Graph>(std::vector<typename TmpGenerators<Graph>::Vertex>(perm, perm + n)));

    //expand orbit
    auto orbit = orbits[stabvertex];
    for (int v = 0; v < n; v++) {
      if (orbits[v] == orbit) {
        TmpGenerators<Graph>::stabilizerOrbits.front().insert(v);
      }
    }
  }
}


template<class Vertex>
std::vector<std::unordered_set<Vertex>> getOrbits(const int* orbits, int n, int* index = NULL, int v = 0) {
  std::vector<std::unordered_set<Vertex>> os(n);
  for (int i = 0; i < n; i++) {
    os[orbits[i]].insert(i);
  }

  std::vector<std::unordered_set<Vertex>> _orbits;
  _orbits.reserve(n);
  for (int i = 0; i < n; i++) {
    if (!os[i].empty()) {
      if (index != NULL && i == orbits[v]) {
        *index = _orbits.size();
      }
      _orbits.push_back(os[i]);

    }
  }
  return _orbits;
}

template<class Vertex>
std::vector<std::unordered_set<Vertex>> getOrbits(const std::vector<int> orbits) {
  return getOrbits<Vertex>(&orbits[0], orbits.size());
}


template<class Graph>
AutomorphismGroup<Graph> getAutomorphisms(const Graph& g) {
  size_t n = num_vertices(g);
  size_t m = (n + WORDSIZE - 1) / WORDSIZE;
  std::vector<int> orbits(n);
  statsblk stats;
  std::vector<int> lab(n);
  std::vector<int> ptn(n);

  internal::TmpGenerators<Graph>::generators = std::deque<std::vector<Automorphism<Graph>>>();
  typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
  internal::TmpGenerators<Graph>::stabilizerOrbits = std::deque<std::unordered_set<Vertex>>();
  internal::TmpGenerators<Graph>::stabilizerVertices = std::deque<Vertex>();
  auto vertex_id = boost::get(boost::vertex_index, g);
  auto vs = vertices(g);

  if (m > 1) {
    //use sparsenauty
    DEFAULTOPTIONS_SPARSEGRAPH(options);
    options.userautomproc = &internal::userautomproc<Graph>;
    sparsegraph nauty_graph;
    nauty_graph.nv = n;
    nauty_graph.nde = 2 * num_edges(g);
    std::vector<int> edges;
    edges.reserve(nauty_graph.nde);
    std::vector<int> degrees(n);
    std::vector<size_t> vertexEdgeIndexStart(n);
    for (auto v_it = vs.first; v_it != vs.second; v_it++) {
      auto v = get(vertex_id, *v_it);
      auto oe = out_edges(*v_it, g);
      vertexEdgeIndexStart[v] = edges.size();
      degrees[v] = out_degree(v, g);
      for (auto e_it = oe.first; e_it != oe.second; e_it++) {
        assert(source(*e_it, g) == v);
        edges.push_back(target(*e_it, g));
      }
    }
    assert(edges.size() == nauty_graph.nde);
    nauty_graph.v = &vertexEdgeIndexStart[0];
    nauty_graph.d = &degrees[0];
    nauty_graph.e = &edges[0];
    nauty_graph.vlen = n;
    nauty_graph.dlen = n;
    nauty_graph.elen = nauty_graph.nde;
    nauty_graph.wlen = 0;
    nausparse_check(WORDSIZE, m, n, NAUTYVERSIONID);
    sparsenauty(&nauty_graph, &lab[0], &ptn[0], &orbits[0], &options, &stats, nullptr);
  } else {
    std::vector<setword> nauty_graph;
    nauty_graph.reserve(n * m);
    DEFAULTOPTIONS_GRAPH(options);
    options.userautomproc = &internal::userautomproc<Graph>;

    int i = 0;
    _unused( i ); //only used in asserts
    for (auto v_it = vs.first; v_it != vs.second; v_it++) {
      auto v = get(vertex_id, *v_it);
      assert(v == i++);
      for (size_t j = 0; j < m; j++) {
        setword w = 0;
        for (size_t k = 0; k < WORDSIZE; k++) {
          auto v2 = j * WORDSIZE + k;
          if (v2 >= n) {
            break;
          }
          if (v2 < n && boost::edge(v, v2, g).second) {
            w |= 1UL << (WORDSIZE - k - 1);
          }
        }
        nauty_graph.push_back(w);
      }
    }
    naugraph_check(WORDSIZE, m, n, NAUTYVERSIONID);
    densenauty(&nauty_graph[0], &lab[0], &ptn[0], &orbits[0], &options, &stats, m, n, nullptr);
  }

  return AutomorphismGroup<Graph>(std::vector<Vertex>(internal::TmpGenerators<Graph>::stabilizerVertices.begin(),
                                                      internal::TmpGenerators<Graph>::stabilizerVertices.end()),
                                  std::vector<std::vector<Automorphism<Graph>>>(
                                      internal::TmpGenerators<Graph>::generators.begin(),
                                      internal::TmpGenerators<Graph>::generators.end()),
                                  std::vector<std::unordered_set<Vertex>>(
                                      internal::TmpGenerators<Graph>::stabilizerOrbits.begin(),
                                      internal::TmpGenerators<Graph>::stabilizerOrbits.end()),
                                  getOrbits<Vertex>(orbits));
}

template<class Vertex>
std::unordered_set<Vertex> getOrbitOf(const std::vector<int>& orbits, Vertex v) {
  std::unordered_set<Vertex> res;
  for (int i = 0; i < orbits.size(); i++) {
    if (orbits[i] == orbits[v]) {
      res.insert(i);
    }
  }
  return res;
}

template<class Graph>
AutomorphismGroup<Graph> getAutomorphismsOfTransitionedGraph(const TransitionedGraph<Graph>& g, bool withStabilizers) {
  //build extended graph
  size_t n = num_vertices(g.getGraph());
  size_t m = num_edges(g.getGraph());
  Graph ext(n + 2 * m);
  auto es = edges(g.getGraph());
  auto edge_id = boost::get(boost::edge_index, g.getGraph());
  auto vertex_id = boost::get(boost::vertex_index, g.getGraph());
  size_t edge_index = 0;
  std::vector<typename boost::graph_traits<Graph>::edge_iterator> edges_by_id(m);
  for (auto e_it = es.first; e_it != es.second; e_it++) {
    auto e = get(edge_id, *e_it);
    edges_by_id[e] = e_it;
    auto v1 = get(vertex_id, source(*e_it, g.getGraph()));
    auto v2 = get(vertex_id, target(*e_it, g.getGraph()));
    size_t ve1 = n + e + (v1 > v2 ? m : 0);
    size_t ve2 = n + e + (v1 > v2 ? 0 : m);
    add_edge(v1, ve1, edge_index++, ext);
    add_edge(ve1, ve2, edge_index++, ext);
    add_edge(ve2, v2, edge_index++, ext);
  }
  for (auto transition: g.getTransitions()) {
    size_t ve1 = n + get(edge_id, transition.getE1()) + (source(transition.getE1(), g.getGraph()) >= transition.getV() &&
                                                      target(transition.getE1(), g.getGraph()) >= transition.getV() ? 0
                                                                                                                    : m);
    size_t ve2 = n + get(edge_id, transition.getE2()) + (source(transition.getE2(), g.getGraph()) >= transition.getV() &&
                                                      target(transition.getE2(), g.getGraph()) >= transition.getV() ? 0
                                                                                                                    : m);
    add_edge(ve1, ve2, edge_index++, ext);
  }

  //calculate automorphisms of extended graph
  auto ext_aut = getAutomorphisms(ext);

  typedef typename boost::graph_traits<Graph>::vertex_descriptor Vertex;
  if (withStabilizers) {
    schreier* gp;    /* This will point to the Schreier structure */
    permnode* gens;  /* This will point to the stored generators */
    newgroup(&gp, &gens, n);
    std::vector<std::vector<int>> generators;
    for (size_t i = 0; i < ext_aut.getStabilizerVertices().size(); i++) {
      for (auto gen: ext_aut.getGenerators()[i]) {
        generators.push_back(std::vector<int>(gen.image.begin(), gen.image.begin() + n));
      }
    }
    for (size_t i = 0; i < generators.size(); i++) {
      addgenerator(&gp, &gens, &generators[i][0], n);
    }

    std::vector<int> base;
    std::set<int> inserted;
    for (int i = 0; i < 2; i++) {
      for (auto v: ext_aut.getStabilizerVertices())
        if (v < n) {
          if (i == 0 && inserted.insert(v).second) {
            base.push_back(v);
          }
        } else {
          //get edge id
          size_t id = v - n;
          if (id >= m) {
            id -= m;
          }
          auto edge = edges_by_id[id];
          int v1 = source(*edge, g.getGraph());
          int v2 = target(*edge, g.getGraph());
          int vu = ((v1 < v2) xor (v - n >= m) xor (i == 1)) ? v1 : v2;
          if (inserted.insert(vu).second) {
            base.push_back(vu);
          }
        }
    }
    for (size_t i = 0; i < n; i++) {
      if (inserted.insert(i).second) {
        base.push_back(i);
      }
    }

    //reform automorphisms of extended graph to automorphisms of original graph
    std::vector<Vertex> stabilizerVertices;
    std::vector<std::vector<Automorphism<Graph>>> endGenerators; //grouped by stabilizers
    std::vector<std::unordered_set<Vertex>> stabilizerOrbits; //the orbit of vertex w_i in stabilizer of w_0,...,w_{i-1}
    std::vector<std::unordered_set<Vertex>> orbits;

    std::vector<std::unordered_set<Vertex>> oldOrbits;
    for (size_t i = 0; i < n; i++) {
      auto orbs = getorbits(&base[0], i, gp, &gens, n);
      int index;
      auto os = getOrbits<Vertex>(orbs, n, &index, base[i]);
      if (i == 0) {
        stabilizerOrbits.push_back(os[index]);
        orbits = os;
      } else if (os != oldOrbits) {
        stabilizerVertices.push_back(base[i - 1]);
        stabilizerOrbits.push_back(os[index]);
      }
      if (os.size() == n) {
        break;
      }
    }

    return AutomorphismGroup<Graph>(stabilizerVertices, endGenerators, stabilizerOrbits, orbits);
  } else {
    std::vector<std::unordered_set<Vertex>> orbits;
    for (auto& orbit: ext_aut.getOrbits()) {
      if (*orbit.begin() < n) {
        orbits.push_back(orbit);
      }
    }
    return AutomorphismGroup<Graph>(std::vector<Vertex>(), std::vector<std::vector<Automorphism<Graph>>>(),
        std::vector<std::unordered_set<Vertex>>(), orbits);
  }
}

#endif //PPM_TESTING_AUTOMORPHISMGROUP_H
