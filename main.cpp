#include <boost/program_options.hpp>
#include <boost/graph/adjacency_list.hpp>
#include "GraphParser.h"
#include "FindPPM.h"
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <sys/resource.h>
#include <signal.h>

typedef boost::property<boost::edge_index_t, std::size_t> EdgeProperty;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS, boost::no_property, EdgeProperty> Graph;

void print_result(const std::deque<PerfectPseudoMatching<Graph>>& matchings, bool minimal_output,
    bool noPseudo = false) {
  size_t num = matchings.size();

  if (!minimal_output && !noPseudo) std::cout << "Total number of matchings: " << num << std::endl;

  bool first_found = false;
  int countPerfectMatching = 0;
  for (auto& matching: matchings) {
    if (matching.isIsPerfectMatching()) {
      countPerfectMatching += 1;
    }
  }

  if (noPseudo) {
    assert(countPerfectMatching == num);
  }

  if (!minimal_output) std::cout << "Total number of perfect matchings: " << countPerfectMatching << std::endl;

  for (auto& prop: PPM::properties) {
    int min = 0;
    int max = 0;
    int minPerfectMatching = 0;
    int maxPerfectMatching = 0;
    const PerfectPseudoMatching<Graph>* found = NULL;
    for (auto& matching: matchings) {
      auto value = matching.getProperty(prop);
      if (value == True) {
        min += 1;
        max += 1;
        if (found == NULL) {
          found = &matching;
        }
        if (matching.isIsPerfectMatching()) {
          minPerfectMatching += 1;
          maxPerfectMatching += 1;
        }
      } else if (value == Unknown) {
        max += 1;
        if (matching.isIsPerfectMatching()) {
          maxPerfectMatching += 1;
        }
      }
    }
    if (noPseudo) {
      assert(min == minPerfectMatching);
      assert(max == maxPerfectMatching);
    }
    if (!minimal_output && !noPseudo) std::cout << "Number of " << PPM::property_names.at(prop) << " matchings: " << min;
    if (max > min) {
      if (!minimal_output && !noPseudo) std::cout << " <= x <= " << max;
    }
    if (found != NULL && !first_found) {
      first_found = true;
      if (!minimal_output)
        std::cout << std::endl << "Example matching: " << *found;
      else {
        switch (prop) {
        case Property::Planar: {
          std::cout << "p";
          return;
        }
        case Property::K5MinorFree: {
          std::cout << "k";
          return;
        }
        case Property::BadK5MinorFree: {
          std::cout << "b";
          return;
        }
        case Property::ContainsCCD: {
          std::cout << "c";
          return;
        }
        }
      }
    }
    if (!minimal_output) std::cout << std::endl;
    if (!minimal_output)
      std::cout << "Number of " << PPM::property_names.at(prop) << " perfect matchings: " << minPerfectMatching;
    if (maxPerfectMatching > minPerfectMatching) {
      if (!minimal_output) std::cout << " <= x <= " << maxPerfectMatching;
    }
    if (!minimal_output) std::cout << std::endl;
  }
  //nothing found!!!!
  if (minimal_output) std::cout << "n";
}

FindPPM getPPMFinder(boost::program_options::variables_map& vm, CPUTimes& times) {
  std::string bad_k5_instance_dir = "";
  if (vm.count("write_bad_K5_instances_dir") > 0) {
    bad_k5_instance_dir = vm["write_bad_K5_instances_dir"].as<std::string>();
  }
  std::string unsatDir = vm.count("unsat_dir") > 0 ? vm["unsat_dir"].as<std::string>() : "";
  if (vm.count("properties") > 0) {
    return FindPPM(&times, vm.count("minimal") > 0, vm.count("recheck_implications") > 0, bad_k5_instance_dir,
                   vm.count("noSym1") == 0, vm.count("noSym2") == 0, vm.count("mip") > 0,
                   vm["properties"].as<std::vector<Property>>(), vm.count("check_dominating_cycles") > 0, unsatDir);
  }
  return FindPPM(&times, vm.count("minimal") > 0, vm.count("recheck_implications") > 0, bad_k5_instance_dir,
                 vm.count("noSym1") == 0, vm.count("noSym2") == 0, vm.count("mip") > 0,
                 vm.count("check_dominating_cycles") > 0, unsatDir);
}

void work_one_graph(std::istream& input, boost::program_options::variables_map& vm, std::string filenamePrefix = "") {
  GraphParser p;
  CPUTimes times;
  std::clock_t c_start = std::clock();
  Graph g = p.parseGraph6<Graph>(input);
  FindPPM finder = getPPMFinder(vm, times);
  bool noPseudo = vm.count("no_pseudo");
  auto perfectPseudoMatchings = finder.findPPM(g, !noPseudo, filenamePrefix);
  times.total_run_time = (double) (std::clock() - c_start) / CLOCKS_PER_SEC;
  bool minimal_output = vm.count("minimal_output") > 0;
  if (vm.count("print_edge_list") > 0 && !minimal_output) {
    auto es = edges(g);
    std::cout << "edge list:" << std::endl << std::endl;
    std::cout << "source;Target;Id" << std::endl;
    auto edge_id = boost::get(boost::edge_index, g);
    for (auto it = es.first; it != es.second; it++) {
      std::cout << source(*it, g) << ";" << target(*it, g) << ";" << get(edge_id, *it) << std::endl;
    }
    std::cout << std::endl;
  }
  if (vm.count("check_dominating_cycles") > 0) {
    std::vector<std::tuple<std::set<int>, int, bool, std::string>> dominatingCycles;
    // first element of tuple is set of claw centers, second is count of dominating cycles with that claw centers,
    // third is true if there is a larger cycle than this one and fourth is a string representation of one of the
    // dominating cycles.
    int numDominating = 0;
    for (auto& matching: perfectPseudoMatchings) {
      std::string representation;
      if (matching.complementIsCycle(representation)) {
        //found a dominating cycle
        numDominating++;
        std::set<int> clawCenters = matching.clawCenters();
        bool found = false;
        bool largerExists = false;
        for(auto& t: dominatingCycles) {
          if (std::includes(std::get<0>(t).begin(), std::get<0>(t).end(), clawCenters.begin(), clawCenters.end())) {
            if (std::get<0>(t).size() == clawCenters.size()) {
              std::get<1>(t)++;
              found = true;
            } else {
              std::get<2>(t) = true;
            }
          } else if (std::includes(clawCenters.begin(), clawCenters.end(), std::get<0>(t).begin(), std::get<0>(t).end())) {
            largerExists = true;
          }
        }
        if (!found) {
          dominatingCycles.push_back(std::make_tuple(clawCenters, 1, largerExists, representation));
        }
      }
    }
    int numStableDominating = 0;
    for(auto& t: dominatingCycles) {
      if (std::get<1>(t) == 1 && !std::get<2>(t)) {
        if (numStableDominating == 0 && !minimal_output) {
          std::cout << "Example SDC: " << std::get<3>(t) << std::endl;
        }
        numStableDominating++;
      }
    }
    if (minimal_output) {
      std::cout << numDominating << ":" << numStableDominating << ":";
    } else {
      std::cout << "Number of dominating cycles: " << numDominating << std::endl;
      std::cout << "Number of stable dominating cycles: " << numStableDominating << std::endl;
    }
  }
  print_result(perfectPseudoMatchings, minimal_output, noPseudo);
  if (minimal_output) {
    std::cout << times.total_run_time;
  } else {
    std::cout << std::endl << times;
  }
}

std::istream& operator>>(std::istream& in, Property& property) {
  std::string token;
  in >> token;

  boost::to_upper(token);

  if (token == "PLANAR") {
    property = Planar;
  } else if (token == "K5MINORFREE") {
    property = K5MinorFree;
  } else if (token == "BADK5MINORFREE") {
    property = BadK5MinorFree;
  } else if (token == "CONTAINSCCD") {
    property = ContainsCCD;
  } else {
    throw boost::program_options::validation_error(boost::program_options::validation_error::invalid_option_value);
  }

  return in;
}

class SignalHandler {
  private:
    static void signalHandler(int _ignored) {
      std::cout << "result:2" << std::endl;
      std::cout << "preptime:" << modellingTime << std::endl;
      std::cout << "solvetime:" << timelimit - modellingTime << std::endl;
      std::cout << "runtime:" << timelimit;
      exit(0);
    }
  public:
    static void setTimeLimit(double timelimit) {
      SignalHandler::timelimit = timelimit;
      modellingTime = 0.0;
      signal(SIGXCPU, signalHandler);
      rlimit rl;
      getrlimit(RLIMIT_CPU, &rl);
      if (rl.rlim_max == RLIM_INFINITY || (rlim_t)timelimit < rl.rlim_max){
        rl.rlim_cur = timelimit;
        if (setrlimit(RLIMIT_CPU, &rl) == -1) {
          std::cerr << "Couldn't set time limit" << std::endl;
          exit(1);
        }
      }
    }
    static double modellingTime;
    static double solvingTime;
    static double timelimit;
};

double SignalHandler::modellingTime = 0.0;
double SignalHandler::solvingTime = 0.0;
double SignalHandler::timelimit = 0.0;

void work_single_instance(const boost::program_options::variables_map& vm) {
  std::string instance_path = vm["instance_path"].as<std::string>();
  std::ifstream input(instance_path);
  GraphParser p;
  auto instance = p.parseSingleInstance<Graph>(input);
  if (vm.count("timelimit") > 0) {
    SignalHandler::setTimeLimit(vm["timelimit"].as<double>());
  }
  bool hasSymmetries = 0;
  std::string unsatFile = vm.count("unsat_dir") > 0 ?
      vm["unsat_dir"].as<std::string>() + "/" + vm["unsat_file"].as<std::string>() : "";
  bool res = containsSUDMinor(*instance.first, *instance.second, SignalHandler::modellingTime,
      SignalHandler::solvingTime,vm.count("noSym1") == 0, vm.count("noSym2") == 0, vm.count("mip") > 0,
      0.0, &hasSymmetries, unsatFile);
  std::cout << "hasSymmetries:" << hasSymmetries << std::endl;
  std::cout << "result:" << res << std::endl;
  std::cout << "preptime:" << SignalHandler::modellingTime << std::endl;
  std::cout << "solvetime:" << SignalHandler::solvingTime << std::endl;
  std::cout << "runtime:" << SignalHandler::modellingTime + SignalHandler::solvingTime;
  delete instance.first;
  delete instance.second;
}

int main(int argc, const char* argv[]) {
  try {
    boost::program_options::options_description desc{"Usage: ppm_testing instance_path [options]\nAllowed options"};
    desc.add_options()("help,h", "Help screen")("instance_path", boost::program_options::value<std::string>(),
                                                "path to instance containing one g6 graph")("instance_string",
                                                                                            boost::program_options::value<std::string>(),
                                                                                            "graph6 string representing the input graph")(
        "minimal", "If set the program stops if a pseudo-matching with the wanted properties was found!")(
        "recheck_implications", "If set the program always checks a property also if it would be implied by another "
                                "property")("first_line", boost::program_options::value<size_t>(),
                                            "The first line index of the input file to read")("last_line",
                                                                                              boost::program_options::value<size_t>(),
                                                                                              "The last line index of the input file to read")(
        "properties", boost::program_options::value<std::vector<Property>>()->multitoken(),
        "The different properties to check for")("minimal_output",
                                                 "If set program only outputs graph-index:{p,k,b,c}run-time")(
        "write_bad_K5_instances_dir", boost::program_options::value<std::string>(), "The directory where bad K5 free"
                                                                                    " instances should be written.")(
        "noSym1", "If set the program does not use sym1 constraints!")("noSym2",
                                                                       "If set the program does not use sym2 constraints!")(
        "singleInstance", "If set the instance path is a single instance file not a graph and the program will "
                          "check the single instance")("mip", "if mip should be used, otherwise sat will be used")
        ("timelimit", boost::program_options::value<double>(),
         "The time limit for the sud-H-minor-solver")
        ("no_pseudo", "If set only perfect matchings are enumerated")
        ("check_dominating_cycles", "Count the (stable) dominating cycles of the graph(s)")
        ("print_edge_list", "Print the edge list")
        ("unsat_dir", boost::program_options::value<std::string>(), "Directory where unsat instances should be stored")
        ("unsat_file", boost::program_options::value<std::string>(), "Filename for unsat in case of singleInstance");


    boost::program_options::positional_options_description pd;
    pd.add("instance_path", 1);

    boost::program_options::variables_map vm;
    store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(pd).run(), vm);
    notify(vm);

    if (vm.count("instance_path") == 0 && vm.count("instance_string") == 0) {
      std::cout << "No instance path or string given!" << std::endl << desc << std::endl;
    } else if (vm.count("help")) {
      std::cout << desc << std::endl;
    } else if (vm.count("singleInstance")) {
      work_single_instance(vm);
    } else {
      if (vm.count("instance_path") > 0) {
        std::string instance_path = vm["instance_path"].as<std::string>();
        boost::filesystem::path p(instance_path);
        std::string instance_name = p.stem().string();
        std::ifstream input(instance_path);

        if (vm.count("first_line")) {
          size_t first_line = vm["first_line"].as<size_t>();
          assert(vm.count("last_line"));
          size_t last_line = vm["last_line"].as<size_t>();
          assert(first_line <= last_line);
          for (size_t i = 0; i < first_line; ++i) {
            input.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
          }
          for (size_t i = first_line; i <= last_line; i++) {
            if (i > first_line) {
              std::cout << std::endl;
              if (vm.count("minimal_output") == 0) {
                std::cout << std::endl << std::endl;
              }
            }
            if (vm.count("minimal_output")) {
              std::cout << i << ":";
            } else {
              std::cout << "Graph index: " << i << std::endl;
            }
            work_one_graph(input, vm, instance_name + "_" + std::to_string(i));
          }
        } else {
          work_one_graph(input, vm, instance_name);
        }
      } else {
        assert(vm.count("instance_string") > 0);
        std::stringstream input(vm["instance_string"].as<const std::string&>());
        work_one_graph(input, vm);
      }
    }
  } catch (const boost::program_options::error& ex) {
    std::cerr << ex.what() << std::endl;
  }
}